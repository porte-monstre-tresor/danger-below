using Godot;
using System;

public class DestructionEffect : Particles2D
{
	[Export]
	public bool SoundEnabled = true;
	
	[Export(PropertyHint.Enum, "Small, Big")]
	public int SoundType = 0;
	
	private AnimationPlayer _animationPlayer = null;
	private AudioStreamPlayer2D _smallAudioStream = null;
	private AudioStreamPlayer2D _bigAudioStream = null;
	
	public override void _Ready()
	{
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
		_smallAudioStream = GetNode("SmallAudioStream") as AudioStreamPlayer2D;
		_bigAudioStream = GetNode("BigAudioStream") as AudioStreamPlayer2D;
	}
	
	public async void Activate()
	{
		Emitting = true;
		if (SoundEnabled)
		{
			switch(SoundType){
				case 0:
					_smallAudioStream.PitchScale = 0.8f + GD.Randf()*0.4f; 
					_animationPlayer.Play("SmallBreakSound");
					break;
				case 1:
					_animationPlayer.Play("BigBreakSound");
					break;
			}
		}
	}
	
	public void CloneInCurrentScene(bool soundEnabled)
	{
		var particle = Duplicate() as DestructionEffect;
		GetNode("/root/World").AddChild(particle);
		particle.GlobalPosition = GlobalPosition;
		particle.SoundEnabled = soundEnabled;
		particle.Activate();
	}
}

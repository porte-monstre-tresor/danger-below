using Godot;
using System;

public class DeathEffect : Sprite
{
	private Timer _timer = null;
	public Particles2D BloodParticles = null;
	public AnimationPlayer Animation = null;
	
	public override void _Ready()
	{
		BloodParticles = GetNode("BloodParticles") as Particles2D;
		Animation = GetNode("AnimationPlayer") as AnimationPlayer;
//		_timer = GetNode("BloodParticles/Timer") as Timer;
	}
	
	public void Activate()
	{
		Visible = true;
		BloodParticles.Emitting = true;
		if (GD.Randf()<0.5f)
		{
			FlipH = true;
		}
	}
	
	public void CloneInCurrentScene(float rotation)
	{
		var deathEffect = Duplicate() as DeathEffect;
		GetNode("/root/World/WallTileMap").AddChild(deathEffect);
		deathEffect.GlobalPosition = GlobalPosition;
		deathEffect.BloodParticles.Rotation = rotation;
		deathEffect.Animation.Play("Effect");
	}

	private void _on_Timer_timeout()
	{
		BloodParticles.SpeedScale = 0;
	}
}

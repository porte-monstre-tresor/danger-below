using Godot;
using System;

public class RustySword : Weapon
{
	[Export]
	protected float spinDuration = 0.1f;
	
	private CollisionShape2D _hitboxCollider = null;
	
	protected override void _setupWeapon()
	{
		toThrow = false;
		toDash = false;
		toSpin = true;
		
		_hitboxCollider = GetNode("Hitbox/CollisionShape2D") as CollisionShape2D;
	}
	
	public async override void Attack()
	{
		active = true;
		EmitSignal("Spin", spinDuration);
		_hitboxCollider.Disabled = false;
		EmitSignal("WeaponUsed");
		_effectAnimationPlayer.Play("ThrowSound");
		
		await ToSignal(GetTree().CreateTimer(spinDuration, false), "timeout");
		_hitboxCollider.Disabled = true;
		
	}
}

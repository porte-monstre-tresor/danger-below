using Godot;
using System;

public class Knife : Weapon
{
	[Export]
	public int Speed = 400;
	
	[Export(PropertyHint.Range, "1,5,1,or_greater")]
	public int Knives = 5;
	
	[Export]
	public float Delay = 0.1f;
	
	private float _initRotation = 0f;
	private bool _used;
	
	private PackedScene _knifeScene = null;
	private CollisionShape2D _hitboxCollider = null;
	private DestructionEffect _destructionEffect = null;
	
	protected override void _setupWeapon()
	{
		toThrow = true;
		toDash = false;
		toSpin = false;
		
		if (!active)
		{
			// Prepare the packed scene to throw the basic sword
			_knifeScene = GD.Load("res://Entities/Weapons/Knife/Knife.tscn") as PackedScene;
			_used = false;
		}
		else {
			// Activate the hitbox
			_hitboxCollider = GetNode("Hitbox/CollisionShape2D") as CollisionShape2D;
			_hitboxCollider.Disabled = false;
			
			_destructionEffect = GetNode("DestructionEffect") as DestructionEffect;
			_used = true;
		}
	}
	
	public override void _PhysicsProcess(float delta)
	{
		if (active)
		{
			// Get the thrown weapon moving
			var newPos = Position;
			newPos.x += Mathf.Cos(_initRotation) * Speed * delta;
			newPos.y += Mathf.Sin(_initRotation) * Speed * delta;
			Position = newPos;
		}
	}
	
	public async override void Attack()
	{
		if (!_used)
		{
			_used = true;
			var projectile = _knifeScene.Instance() as Knife;
			projectile.RotationDegrees = GlobalRotationDegrees;
			projectile._initRotation = Mathf.Deg2Rad(GlobalRotationDegrees-90);
			projectile.Position = GlobalPosition;
			projectile.active = true;
			
	//		Add it to the world
			GetNode("/root").AddChild(projectile);
			
			_effectAnimationPlayer.Play("ThrowSound");
			
			for (int i=0; i<Knives-1; i++)
			{
				await ToSignal(GetTree().CreateTimer(Delay), "timeout");
				projectile = _knifeScene.Instance() as Knife;
				projectile.RotationDegrees = GlobalRotationDegrees;
				projectile._initRotation = Mathf.Deg2Rad(GlobalRotationDegrees-90);
				projectile.Position = GlobalPosition;
				projectile.active = true;
				GetNode("/root").AddChild(projectile);
			
				_effectAnimationPlayer.Play("ThrowSound");
			}
			
			EmitSignal("WeaponUsed");
			_used = false;
		}
	}

	private void _on_WallDetectorBodyEntered(object body)
	{
		// If thrown, destroyed when a wall collide with the WallDetector node
		if (active)
		{
			_destructionEffect.CloneInCurrentScene(true);
			QueueFree();
		}
	}
	
	private void _on_WallDetectorAreaEntered(object area)
	{
		// If thrown, destroyed when an enemy collide with the WallDetector node
		if (active)
		{
			_destructionEffect.CloneInCurrentScene(true);
			QueueFree();
		}
	}
}




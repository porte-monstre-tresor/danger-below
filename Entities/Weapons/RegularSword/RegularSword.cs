using Godot;
using System;

public class RegularSword : Weapon
{
	[Export]
	protected int dashSpeed = 400;
	
	[Export]
	protected float dashDuration = 0.1f; 
	
	[Export]
	protected float spinDuration = 0.1f;
	
	
	private CollisionShape2D _hitboxCollider = null;
	
	protected override void _setupWeapon()
	{
		toThrow = false;
		toDash = true;
		toSpin = true;
		
		_hitboxCollider = GetNode("Hitbox/CollisionShape2D") as CollisionShape2D;
	}
	
	public async override void Attack()
	{
		active = true;
		EmitSignal("Dash", dashSpeed, dashDuration);
		EmitSignal("Spin", spinDuration);
		_hitboxCollider.Disabled = false;
		_effectAnimationPlayer.Play("ThrowSound");
		
		await ToSignal(GetTree().CreateTimer(Math.Max(dashDuration, spinDuration), false), "timeout");
		_hitboxCollider.Disabled = true;
		
		EmitSignal("WeaponUsed");
			
	}
}

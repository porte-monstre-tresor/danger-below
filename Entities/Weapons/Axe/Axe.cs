using Godot;
using System;

public class Axe : Weapon 
{
	[Export]
	protected int Speed = 200;
	
	[Export]
	protected int BreakingPoint = 3;
	
	private float _initRotation = 0f;
	
	private PackedScene _weaponScene = null;
	private CollisionShape2D _hitboxCollider = null;
	private DestructionEffect _destructionEffectWood = null;
	private DestructionEffect _destructionEffectSteel = null;
	
	protected override void _setupWeapon()
	{	
		toThrow = true;
		toDash = false;
		toSpin = false;
		
		if (!active)
		{
			// Prepare the packed scene to throw the basic sword
			_weaponScene = GD.Load("res://Entities/Weapons/Axe/Axe.tscn") as PackedScene;
		}
		else {
			// Play the attack animation
			var ap = GetNode("AnimationPlayer") as AnimationPlayer;
			ap.Play("Attack");
			
			// Activate the hitbox
			_hitboxCollider = GetNode("Hitbox/CollisionShape2D") as CollisionShape2D;
			_hitboxCollider.Disabled = false;
			
			_destructionEffectWood = GetNode("DestructionEffectWood") as DestructionEffect;
			_destructionEffectSteel = GetNode("DestructionEffectSteel") as DestructionEffect;
		}
	}
	
	public override void _PhysicsProcess(float delta)
	{
		if (active)
		{
			// Get the thrown weapon moving
			var newPos = Position;
			newPos.x += Mathf.Cos(_initRotation) * Speed * delta;
			newPos.y += Mathf.Sin(_initRotation) * Speed * delta;
			Position = newPos;
		}
	}
	
	public override void Attack()
	{
		var projectile = _weaponScene.Instance() as Axe;
		projectile.RotationDegrees = GlobalRotationDegrees-90;
		projectile._initRotation = projectile.Rotation;
		projectile.Position = GlobalPosition;
		projectile.active = true;
		
//		Add it to the world
		GetNode("/root").AddChild(projectile);
		
		EmitSignal("WeaponUsed");
			
		_effectAnimationPlayer.Play("ThrowSound");
	}

	private void _on_WallDetectorBodyEntered(object body)
	{
		// If throw, destroyed when a wall collide with the WallDetector node
		if (active)
		{
			_destructionEffectWood.CloneInCurrentScene(true);
			_destructionEffectSteel.CloneInCurrentScene(false);
			QueueFree();
		}
	}

	private void _on_WallDetectorAreaEntered(object area)
	{
		if (active)
		{
			BreakingPoint -= 1;
			if (BreakingPoint<1)
			{
				_destructionEffectWood.CloneInCurrentScene(false);
				_destructionEffectSteel.CloneInCurrentScene(false);
				QueueFree();
			}
		}
	}
}




using Godot;
using System;

public class Nail : Weapon
{
	[Export]
	public int Speed = 400;
	
	public float InitRotation = 0f;
	public bool IsThrownByPlayer = false;
	
	private Hitbox _hitbox = null;
	private Area2D _wallDetector = null;

	protected override void _setupWeapon()
	{
		toThrow = true;
		toDash = false;
		toSpin = false;
		
		_hitbox = GetNode("Hitbox") as Hitbox;
		_hitbox.CollisionMask = (IsThrownByPlayer) ? (uint)16 : (uint)4;
		
		_wallDetector = GetNode("WallDetector") as Area2D;
		_wallDetector.CollisionMask = (IsThrownByPlayer) ? (uint)17 : (uint)5;
	}

	public override void _PhysicsProcess(float delta)
	{
		var newPos = Position;
		newPos.x += Mathf.Cos(InitRotation) * Speed * delta;
		newPos.y += Mathf.Sin(InitRotation) * Speed * delta;
		Position = newPos;
	}

	private void _on_WallDetector_area_entered(object area)
	{
		QueueFree();
	}


	private void _on_WallDetector_body_entered(object body)
	{
		QueueFree();
	}
}


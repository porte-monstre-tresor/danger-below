using Godot;
using System;

public class NailedBat : Weapon
{
	private Hitbox _hitbox = null;
	
	private PackedScene _nailScene = null;
	
	[Export]
	public bool IsHandledByPlayer = false;

	protected override void _setupWeapon()
	{
		_hitbox = GetNode("Hitbox") as Hitbox;
		_hitbox.CollisionMask = (IsHandledByPlayer) ? (uint)16 : (uint)4;
		
		_nailScene = GD.Load("res://Entities/Weapons/NailedBat/Nail.tscn") as PackedScene;
	}

	public override void Attack()
	{
		_throwNail(GlobalRotationDegrees);
		_throwNail(GlobalRotationDegrees-10);
		_throwNail(GlobalRotationDegrees+10);
		_throwNail(GlobalRotationDegrees-5);
		_throwNail(GlobalRotationDegrees+5);
		
		EmitSignal("WeaponUsed");
			
		_effectAnimationPlayer.Play("ThrowSound");
	}
	
	private void _throwNail(float angle)
	{
		var projectile = _nailScene.Instance() as Nail;
		projectile.IsThrownByPlayer = IsHandledByPlayer;
		projectile.RotationDegrees = angle;
		projectile.InitRotation = Mathf.Deg2Rad(angle-90);
		projectile.Position = GlobalPosition;
		GetNode("/root").AddChild(projectile);
	}
}

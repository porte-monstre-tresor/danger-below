using Godot;
using System;

public class Weapon : Node2D
{	
	[Signal]
	delegate void WeaponUsed();
	
	[Signal]
	delegate void Spin(float duration);
	
	[Signal]
	delegate void Dash(int speed, float duration);
	
	protected AnimationPlayer _effectAnimationPlayer = null;
	
	public override void _Ready()
	{
		_effectAnimationPlayer = GetNode("EffectAnimationPlayer") as AnimationPlayer;
		_setupWeapon();
	}
	
	protected virtual void _setupWeapon() {}
	
	public bool toThrow = false; 
	public bool toDash = false;
	public bool toSpin = false;
	
	protected bool active = false;
	
	public virtual void Attack() {}
}

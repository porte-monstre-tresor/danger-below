using Godot;
using System;

public class Spear : Weapon
{
	[Export]
	public int Speed = 400;
	
	[Export]
	public bool IsHandledByPlayer = false;
	
	private float _initRotation = 0f;
	private bool _used;
	
	private PackedScene _spearScene = null;
	
	private Hitbox _hitbox = null;
	private CollisionShape2D _hitboxCollider = null;
	private DestructionEffect _destructionEffect = null;
	
	protected override void _setupWeapon()
	{
		toThrow = true;
		toDash = false;
		toSpin = false;
		
		if (!active)
		{
			// Prepare the packed scene to throw the basic sword
			_spearScene = GD.Load("res://Entities/Weapons/Spear/Spear.tscn") as PackedScene;
			_used = false;
		}
		else {
			// Activate the hitbox
			_hitbox = GetNode("Hitbox") as Hitbox;
			_hitbox.CollisionMask = (IsHandledByPlayer) ? (uint)16 : (uint)4;
			
			_hitboxCollider = GetNode("Hitbox/CollisionShape2D") as CollisionShape2D;
			_hitboxCollider.Disabled = false;
			
			_destructionEffect = GetNode("DestructionEffect") as DestructionEffect;
			_used = true;
		}
	}
	
	public override void _PhysicsProcess(float delta)
	{
		if (active)
		{
			// Get the thrown weapon moving
			var newPos = Position;
			newPos.x += Mathf.Cos(_initRotation) * Speed * delta;
			newPos.y += Mathf.Sin(_initRotation) * Speed * delta;
			Position = newPos;
		}
	}
	
	public async override void Attack()
	{
		if (!_used)
		{
			_used = true;
			var projectile = _spearScene.Instance() as Spear;
			projectile.IsHandledByPlayer = IsHandledByPlayer;
			projectile.RotationDegrees = GlobalRotationDegrees;
			projectile._initRotation = Mathf.Deg2Rad(GlobalRotationDegrees-90);
			projectile.Position = GlobalPosition;
			projectile.active = true;
			
	//		Add it to the world
			GetNode("/root").AddChild(projectile);
			
			EmitSignal("WeaponUsed");
			_used = false;
			
			_effectAnimationPlayer.Play("ThrowSound");
		}
	}

	private void _on_WallDetector_body_entered(object body)
	{
		// If thrown, destroyed when a wall collide with the WallDetector node
		if (active)
		{
			_destructionEffect.CloneInCurrentScene(true);
			QueueFree();
		}
	}
}


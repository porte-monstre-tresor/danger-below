using Godot;
using System;

public class SoftCollision : Area2D
{	
	public bool IsColliding()
	{
//		Check the other soft collision areas that could collide
		var areas = GetOverlappingAreas();
		return ( areas.Count > 0 );
	}
	
	public Vector2 GetPushVector()
	{
		var areas = GetOverlappingAreas();
		var pushVector = Vector2.Zero;
		if (IsColliding())
		{
//			Get pushed by the first colliding soft collision area
			var area = areas[0] as Node2D;
			
//			Compute the direction of the collider and normalize it
			pushVector = area.GlobalPosition.DirectionTo(GlobalPosition);
			pushVector = pushVector.Normalized();
		}
		return pushVector;
	}
}

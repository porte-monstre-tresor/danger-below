using Godot;
using System;

// General idea: 
// 1) if the player is in the vision area, aim him with the ray and check for no obstacle
// 2) if the player was seen but is not anymore, aim its traces with the ray and check for no obstacle
// 3) if the player gets out of the smelling range, forget him

public class PlayerDetectionRay : RayCast2D
{	
	[Signal]
	delegate void TargetAcquired();
	
	[Signal]
	delegate void TargetLost();
	
	private Area2D _visionArea = null;
	
	public Vector2 Target = Vector2.Zero;
	public bool IsTargetPlayer = false;
	
	private Player _player = null;
	private Fart _lastSmell = null;
	private bool _hasTarget = false;
	private bool _inVisionArea = false;
	
	public override void _Ready()
	{
		_visionArea = GetNode("VisionArea") as Area2D;
	}
	
	public override void _PhysicsProcess(float delta)
	{
		if (_player != null)
		{
//			Case 1: player in zone and we can see him
			if (_inVisionArea && _canSeePlayer())
			{
//				Player never seen: we target him
				if (!_hasTarget) {
					_hasTarget = true;
					EmitSignal("TargetAcquired");
				}
//				Update the target
				Target = _player.GlobalPosition;
				IsTargetPlayer = true;
			}
//			Case 2: player in zone and targeted, but we don't see him anymore
			else if (_hasTarget)
			{
//				If we can smell the traces: go for it
				if (_canSmellPlayer())
				{
					IsTargetPlayer = false;
					Target = _lastSmell.GlobalPosition;
				}
//				Else, we lost him, probably rats
				else
				{
					_hasTarget = false;
					EmitSignal("TargetLost");
				}
			}
		}
	}
	
	public void DirectTowards(Vector2 direction)
	{
		_visionArea.Rotation = direction.Angle() - Mathf.Pi/2;
	}
	
	private bool _canSeePlayer()
	{
//		If there is a player in range
		if (!_player.Hidden)
		{
//			Point the ray to it
			CastTo = _player.GlobalPosition - GlobalPosition;
			ForceRaycastUpdate();
			
//			If there is no obstacle to block the enemy's vision, we can see him
			if (!IsColliding())
			{
				return true;
			}
		}
		return false;
	}
	
	private bool _canSmellPlayer()
	{
//		If there is a player in range
		if (_player!=null)
		{
//			We check all the scents left by the player
//			NB: We start by the more recent to avoid going backward
			for (int i=_player.FartTrail.Count-1; i>=0; i--)
			{
//				Point the ray on the fart
				CastTo = _player.FartTrail[i].GlobalPosition - GlobalPosition;
				ForceRaycastUpdate();
				
//				If nothing blocks the ray to the scent, we can follow it
				if (!IsColliding())
				{
					_lastSmell = _player.FartTrail[i] as Fart;
					return true;
				}
			}
		}
		_lastSmell = null;
		return false;
	}

	private void _on_VisionArea_body_entered(object body)
	{
		if (body is Player player)
		{
			_player = player;
			_inVisionArea = true;
		}
	}

	private void _on_VisionArea_body_exited(object body)
	{
		if (body is Player player)
		{
			_inVisionArea = false;
		}
	}
	
	private void _on_SmellArea_body_entered(object body)
	{
		if (body is Player player && _player == null)
		{
			_player = player;
		}
	}
	private void _on_SmellArea_body_exited(object body)
	{
		if (body is Player player && _player != null)
		{
			_player = null;
		}
	}
}

// Note: Basic following mechanics
// - Check the possibilities with a navigation 2D






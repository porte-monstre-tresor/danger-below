using Godot;
using System;

public class DarkExit : Sprite
{
	[Signal]
	delegate void ExitTaken();

	private void _on_Area2D_body_entered(object body)
	{
		if (body is Player)
		{
			EmitSignal("ExitTaken");
		}
	}
}




using Godot;
using System;

public class Hitbox : Area2D
{
	[Export]
//	Default damages of a hitbox
	public int Damage = 1;
	
	[Export]
//	Default strength of a knock back of a hitbox
	public int KnockbackStrength = 0;
	
	[Export]
	public String SourceOfDamage;
}

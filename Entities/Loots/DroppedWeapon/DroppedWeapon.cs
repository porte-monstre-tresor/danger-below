using Godot;
using System;

public class DroppedWeapon : Loot
{
	[Export]
	public String WeaponName = "Knife";
	
	private Weapons _Weapons = null;
	
	private Sprite _sprite = null;

	protected override void _setupLoot()
	{
		_Weapons = GetNode("/root/Weapons") as Weapons;
		
		var spriteContainer = GetNode("Sprite") as Sprite;
		foreach (Sprite child in spriteContainer.GetChildren())
		{
			child.Visible = false;
		}
		
		_sprite = GetNode("Sprite/"+WeaponName) as Sprite;
		
		_sprite.Visible = true;
	}
	
	protected override void GetLooted()
	{
		_Weapons.InsertNext(WeaponName);
	}
}


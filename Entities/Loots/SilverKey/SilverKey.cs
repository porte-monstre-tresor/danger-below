using Godot;
using System;

public class SilverKey : Loot 
{
	private Stats _PlayerStats = null;
	
	protected override void _setupLoot()
	{
		_PlayerStats = GetNode("/root/PlayerStats") as Stats;
	}
	
	protected override void GetLooted()
	{
		_PlayerStats.OffsetInventory("SilverKey", 1);
	}
}



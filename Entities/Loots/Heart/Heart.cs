using Godot;
using System;

public class Heart : Loot
{
	private Stats _PlayerStats = null;
	
	protected override void _setupLoot()
	{
		_PlayerStats = GetNode("/root/PlayerStats") as Stats;
	}
	
	protected override void GetLooted()
	{
		if (_PlayerStats.Health < _PlayerStats.MaxHealth - 1)
		{
			_PlayerStats.Health += 2;
		}
		else
		{
			_PlayerStats.Health = _PlayerStats.MaxHealth;
		}
		
	}
}

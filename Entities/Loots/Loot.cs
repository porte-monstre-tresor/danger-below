using Godot;
using System;

public class Loot : KinematicBody2D
{
	[Export]
	protected int _maxSpeed = 300;
	
	[Export]
	protected int _acceleration = 1000;
	
	protected Area2D _lootArea = null;
	protected AnimationPlayer _animationPlayer = null;
	protected AudioStreamPlayer _audioStream = null;
	
	protected Player _target = null;
	protected Vector2 _velocity = Vector2.Zero;
	
	public bool Locked = false;
	
	public override void _Ready()
	{
		_lootArea = GetNode("LootArea") as Area2D;
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
		_audioStream = GetNode("AudioStreamPlayer") as AudioStreamPlayer;
		_audioStream.PitchScale = 1.1f + GD.Randf()*0.5f;
		
		_setupLoot();
	}
	
	protected virtual void _setupLoot(){}
	
	public override void _PhysicsProcess(float delta)
	{
		if (_target != null && !Locked)
		{
			var direction = (_target.GlobalPosition - GlobalPosition).Normalized();
			_velocity = _velocity.MoveToward(direction * _maxSpeed, _acceleration * delta);
			_velocity = MoveAndSlide(_velocity);
		}
	}
	
	public void Unlock()
	{
		if (_target != null)
		{
			Locked = false;
			if (_lootArea.OverlapsBody(_target))
			{
				_animationPlayer.Play("Looted");
			}
		}
	}
	
	private void _on_DetectArea_body_entered(object body)
	{
		if (body is Player player && _target == null)
		{
			_target = player;
		}
	}

	private void _on_LootArea_body_entered(object body)
	{
		if (body is Player && !Locked)
		{
			_animationPlayer.Play("Looted");
		}
	}
	
	private void _on_DetectArea_body_exited(object body)
	{
		if (body is Player player && _target==player)
		{
			_target = null;
		}
	}	
	
	protected virtual void GetLooted() {}
}

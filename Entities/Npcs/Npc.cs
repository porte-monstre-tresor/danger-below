using Godot;
using System;

public class Npc : KinematicBody2D
{	
//	Note on NPCs sprite sheets: by default, 4 front idle, 4 back idle, 4 front run, 4 back run, 1 front hurt, 1 back hurt 
	
	protected DialogUI _dialogUI = null;
	protected AnimationPlayer _animationPlayer = null;
	protected AnimationTree _animationTree = null;
	protected AnimationNodeStateMachinePlayback _animationState = null;
	protected PlayerDetectionRay _playerDetectionRay = null;
	
	protected Player _target = null;
	
	public override void _Ready()
	{
		_dialogUI = GetNode("UIPanel/DialogUI") as DialogUI;
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
		_animationTree = GetNode("AnimationTree") as AnimationTree;
		_animationState = _animationTree.Get("parameters/playback") as AnimationNodeStateMachinePlayback;
		_playerDetectionRay = GetNode("PlayerDetectionRay") as PlayerDetectionRay;
		
		_setupNpc();
	}
	
	protected virtual void _setupNpc() {}
	
	public override void _Process(float delta)
	{
		if (_playerDetectionRay.IsTargetPlayer)
		{
			var _direction = GlobalPosition.DirectionTo(_playerDetectionRay.Target);
			_playerDetectionRay.DirectTowards(_direction);
			_animationTree.Set("parameters/Idle/blend_position", _direction);
		}
	}
	
	protected virtual void _talk() {}
	
	protected virtual void _additionalInteractibleEffects() {}
	
	protected virtual void _additionalUninteractibleEffects() {}
	
	private void _ignore()
	{
		_target.Disconnect("Ignore", this, "_ignore");
		_target.Disconnect("Interact", this, "_talk");
		_target = null;
		_animationState.Travel("Idle");
		_dialogUI.Stop();
		
		_extendsIgnore();
	}
	
	protected virtual void _extendsIgnore() {}

	private void _on_InteractionArea_body_entered(object body)
	{
		if (body is Player player && _target == null)
		{
			_target = player;
			player.SetInteractible(true);
			player.Connect("Ignore", this, "_ignore");
			player.Connect("Interact", this, "_talk");
			
			_additionalInteractibleEffects();
		}
	}

	private void _on_InteractionArea_body_exited(object body)
	{
		if (body is Player player && _target==player)
		{
			_ignore();
			player.SetInteractible(false);
			
			_additionalUninteractibleEffects();
		}
	}
}


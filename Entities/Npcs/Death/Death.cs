using Godot;
using System;

public class Death : Npc
{
	public String DialogFile;
	private DestructionEffect _smokeEffect = null;
	
	protected override void _setupNpc()
	{
		_smokeEffect = GetNode("SmokeEffect") as DestructionEffect;
	}
	
	protected override void _talk()
	{
		_dialogUI.Play(DialogFile);
	}

	public void Destroy()
	{
		_smokeEffect.CloneInCurrentScene(false);
		QueueFree();
	}
}



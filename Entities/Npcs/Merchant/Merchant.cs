using Godot;
using System;

public class Merchant : Npc
{
	public override void _Process(float delta)
	{
		if (_playerDetectionRay.IsTargetPlayer)
		{
			var direction = GlobalPosition.DirectionTo(_playerDetectionRay.Target);
			_playerDetectionRay.DirectTowards(direction);
			_animationTree.Set("parameters/Idle/blend_position", direction);
			_animationTree.Set("parameters/Close/blend_position", direction);
			_animationTree.Set("parameters/Open/blend_position", direction);
			_animationTree.Set("parameters/Talk/blend_position", direction);
		}
	}
	
	protected override void _talk()
	{
		_dialogUI.Play("res://Entities/Npcs/Merchant/merchant_introduction.json");
	}
	
	protected override void _additionalInteractibleEffects() 
	{
		var direction = GlobalPosition.DirectionTo(_target.GlobalPosition);
		_animationState.Travel("Talk");
	}
	
	protected override void _additionalUninteractibleEffects() 
	{
		_animationState.Travel("Idle");
	}
}

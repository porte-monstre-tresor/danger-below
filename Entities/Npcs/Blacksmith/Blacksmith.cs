using Godot;
using System;

public class Blacksmith : Npc
{
	private ShopUI _shopUI = null;
	
	public String DialogFile = "res://Entities/Npcs/Blacksmith/blacksmith_default.json";
	
//	[Export(PropertyHint.Enum, "Npc, Shop")]
//	private int _status = 0;

	protected override void _setupNpc()
	{
		_shopUI = GetNode("UIPanel/ShopUI") as ShopUI;
	}
	
	protected override void _talk()
	{
		if (!_shopUI.Visible)
		{
			_dialogUI.Play(DialogFile);
		}
	}
	
	protected override void _extendsIgnore()
	{
		_shopUI.Visible = false;
	}

	private void _on_DialogUI_DialogEnded()
	{
		_shopUI.Visible = true;
	}
}

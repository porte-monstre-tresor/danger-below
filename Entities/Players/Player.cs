using Godot;
using System;
using System.Collections.Generic;

public class Player : KinematicBody2D
{
// ############################################################################
//	STATE MACHINE
	enum State {
		Move,
		Dash,
		Attack,
		Dead
	}
	private State _state = State.Move;
	
// ############################################################################
//	VARS

//	For interactions
	[Signal]
	delegate void Interact();
	private bool _interactible = false;
	
	[Signal]
	delegate void Ignore();

//	explicit
	[Export]
	protected int _acceleration = 1000;
	
//	De-acceleration
	[Export]
	protected int _friction = 1000;
	
//	Max running speed
	[Export]
	protected int _maxSpeed = 150;
	
//	Divide the knockback
	[Export]
	protected int _weight = 5;
	
//	Can the player be moved ?
	[Export]
	protected bool _unmovable = false;
	
// Can the player take damage ?
	[Export]
	protected bool _invincible = false;
	
	[Export]
	protected bool _armed = true;
	
	[Export]
	protected float _recoveryTime = 0.5f;
	
	public bool Hidden = false;
	
//	Explicit, velocity at the t-time of the player
	private Vector2 _velocity = Vector2.Zero;
	
//	Angle of the character-pointer line
	private float _targetAngle = 0.0f;
	
//	Dash vars
	private float _dashAngle = 0.0f;
	private int _dashSpeed = 0;
	private float _dashDuration = 0.0f;
	
//	Children nodes used
	private AnimationPlayer _animationPlayer = null;
	private AnimationPlayer _effectAnimationPlayer = null;
	private AnimationTree _animationTree = null;
	private AnimationNodeStateMachinePlayback _animationState = null;
	private CollisionShape2D _collisionShape = null;
	private Position2D _weaponRotater = null;
	private Position2D _weaponsSet = null;
	private Weapon _weapon = null;
	
//	Singletons
	protected Stats _Stats = null;
	private Score _Score = null;
	private Weapons _Weapons = null;
	private Looter _Looter;

//	Trail related stuff
	private PackedScene _fartScene = null;
	public Godot.Collections.Array<Fart> FartTrail = new Godot.Collections.Array<Fart>();
	
	public override void _Ready()
	{
		// True random (no fixed seed as it's default in Godot)
		GD.Randomize();
		
//		Get the children nodes
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
		_animationTree = GetNode("AnimationTree") as AnimationTree;
		_animationState = _animationTree.Get("parameters/playback") as AnimationNodeStateMachinePlayback;
		_effectAnimationPlayer = GetNode("EffectAnimationPlayer") as AnimationPlayer;
		_collisionShape = GetNode("CollisionShape2D") as CollisionShape2D;
		_weaponRotater = GetNode("WeaponRotater") as Position2D;
		
//		Load the scenes to be instanced
		_fartScene = GD.Load("res://Entities/Players/Fart.tscn") as PackedScene;
		
//		Singletons
		_Stats = GetNode("/root/PlayerStats") as Stats;
		_Weapons = GetNode("/root/Weapons") as Weapons;
		_Score = GetNode("/root/Score") as Score;
		_Looter = GetNode("/root/Looter") as Looter;
		
//		Score setup
		_Score.StartTimeCounter();
		
//		Player Stats
		_Stats.Connect("NoHealth", this, "_onDeath");
		
//		Weapons
		_weaponsSet = _weaponRotater.GetNode("WeaponHandle") as Position2D;
		_Weapons.Connect("EquipWeapon", this, "_equipWeapon");
		_Weapons.Connect("UnequipWeapon", this, "_unequipWeapon");
		
		_initWeaponsSet();
		
		if (_armed)
		{
			_Weapons.GetFirstWeapon();
		}
		
	}
	
	public override void _PhysicsProcess(float delta)
	{
//		Actions based on the current state
		switch(_state)
		{
			case State.Move:
				_moveState(delta);
				break;
			case State.Dash:
				_dash();
				break;
			case State.Dead:
				break;
		}
		
		_targetAngle = GetAngleTo(GetGlobalMousePosition());
		_lookAt();
		_rotateWeapon(delta);
	}
	
	private void _moveState(float delta)
	{
//		Get the player's input
		var _inputVector = Vector2.Zero;
		_inputVector.x = Input.GetActionStrength("right") - Input.GetActionStrength("left");
		_inputVector.y = Input.GetActionStrength("down") - Input.GetActionStrength("up");
		_inputVector = _inputVector.Normalized();
		
		
//		The player is running: update animation + velocity increased
		if (_inputVector != Vector2.Zero)
		{
			_animationTree.Set("parameters/Idle/blend_position", _inputVector);
			_animationTree.Set("parameters/Run/blend_position", _inputVector);
			_animationTree.Set("parameters/Hurt/blend_position", _inputVector);
			_animationState.Travel("Run");
			_velocity = _velocity.MoveToward(_inputVector * _maxSpeed, _acceleration * delta);
		}
//		The player is still: animation + velocity decreased
		else
		{
			_animationState.Travel("Idle");
			_velocity = _velocity.MoveToward(Vector2.Zero, _friction * delta);
		}
		
		if (Input.IsActionJustPressed("attack") && _Weapons.IsEquiped && _armed)
		{
			_weapon.Attack();
		}
		if (Input.IsActionJustPressed("interact") && _interactible)
		{
			EmitSignal("Interact");
		}
		
//		Move accordingly to the velocity
		_move();
	}
	
	private void _move()
	{
//		Compute the movement
		_velocity = MoveAndSlide(_velocity);
	}
	
	private void _lookAt()
	{
//		Is the mouse on the left of the player
		var left = _targetAngle>Mathf.Pi/2 || _targetAngle<Mathf.Pi/-2;
	}
	
	private void _rotateWeapon(float delta)
	{
//		WIP: instant rotation, will do clean inerty mechanics later
		_weaponRotater.Rotation = _targetAngle;
	}

	private async void _dash()
	{
//		Dash movement
		_velocity.x = Mathf.Cos(_dashAngle) * _dashSpeed;
		_velocity.y = Mathf.Sin(_dashAngle) * _dashSpeed;
		
//		The player can't be hit during a dash
		_invincible = true;
		
//		Make it move
		_move();
		
		await ToSignal(GetTree().CreateTimer(_dashDuration, false), "timeout");
		
//		Reinitialize the vars
		_dashAngle = 0.0f;
		_dashSpeed = 0;
		_dashDuration = 0.0f;
		_invincible = false;
		
//		Back to classic movements
		_state = State.Move;
	}
	
	private async void _spin(float duration)
	{
		// 	Spin animation
		_animationPlayer.PlaybackSpeed = 1f/duration;
		_animationPlayer.Play("Spin");
		_invincible = true;
		await ToSignal(GetTree().CreateTimer(duration, false), "timeout");
		_invincible = false;
		
		_animationPlayer.PlaybackSpeed = 1f;
	}
	
	private void _initWeaponsSet()
	{
//		Turn all the weapons in hand unused => invisible
		foreach (Weapon weapon in _weaponsSet.GetChildren())
		{
			weapon.Visible = false;
		}
	}
	
	private void _equipWeapon(String name)
	{
		_weapon = _weaponsSet.GetNode(name) as Weapon;
		_weapon.Visible = true;
		
//		Connect the signals of the weapon
		_weapon.Connect("WeaponUsed", this, "_changeWeapon");
		if (_weapon.toDash) { _weapon.Connect("Dash", this, "_onDash"); }
		if (_weapon.toSpin) { _weapon.Connect("Spin", this, "_onSpin"); }
	}
	
	private void _unequipWeapon()
	{
		_weapon.Disconnect("WeaponUsed", this, "_changeWeapon");
		if (_weapon.toDash) { _weapon.Disconnect("Dash", this, "_onDash"); }
		if (_weapon.toSpin) { _weapon.Disconnect("Spin", this, "_onSpin"); }
		
		_Weapons.IsEquiped = false;
		
		
		if (!_Weapons.UsingDefault)
		{
			_weapon.Visible = false;
		}
	}
	
	private void _equipDefaultWeapon()
	{
		GD.Print("default weapons");
	}
	
	public void SetInteractible(bool val)
	{
		if (_interactible && val)
		{
			EmitSignal("Ignore");
		}
		_interactible = val;
	}
	
	public void EraseFromGame()
	{
		_Score.StopTimeCounter();
		
		_state = State.Dead;
		
//		WIP: Nasty workaround
		var hurtbox = GetNode("Hurtbox/CollisionShape2D") as CollisionShape2D;
		hurtbox.SetDeferred("disabled", true);
		SetDeferred("collision_layer", 0);
		
		Visible = false;
	}
	
// ############################################################################
// SIGNALS

	private void _changeWeapon()
	{
		_unequipWeapon();
		_Score.WeaponsUsed += 1;
		_Weapons.GetNextWeapon();
	}

	private void _onDash(int speed, float duration)
	{
		_state = State.Dash;
		_dashAngle = _targetAngle;
		_dashSpeed = speed;
		_dashDuration = duration;
	}
	
	private void _onSpin(float duration)
	{
		_spin(duration);
	}

	private async void _onHurtboxAreaEntered(object area)
	{
//		Check if the collider is really a hitbox
		if (area is Hitbox hitbox)
		{
//			Resolve damages and knockback
			if (!_unmovable)
			{
				var direction = hitbox.GlobalPosition.DirectionTo(GlobalPosition);
				_velocity = hitbox.KnockbackStrength * direction / _weight;
			}
			
			if (!_invincible)
			{
				_Score.HitsTaken += 1;
				_Score.KilledBy = hitbox.SourceOfDamage;
				_Stats.Health -= hitbox.Damage;
				
				_invincible = true;
//				_animationState.Travel("Hurt");
				if (_Stats.Health > 0)
				{
					_effectAnimationPlayer.Play("Hurt");
					await ToSignal(GetTree().CreateTimer(_recoveryTime, false), "timeout");
				}
				_invincible = false;
			}
			
//			Animation
//			_animationState.Travel("Hurt");
		}
	}

	private void _onButtTimeout()
	{
//		Create a fart node, independant but related to the player, on regular basis
		var fart = _fartScene.Instance() as Fart;
		fart.Player = this;
		fart.GlobalPosition = GlobalPosition;
		
//		Add it to the world and in the array of the player
		GetNode("/root/World").AddChild(fart);
		FartTrail.Add(fart);
	}
	
	private void _onDeath(Godot.Collections.Array<String> drops)
	{
		_Looter.DropItemAt(drops, GetParent().GetPath(), GlobalPosition);
		EraseFromGame();
		
	}
}

using Godot;
using System;

public class ItemHolder : Node2D
{
	[Export]
	public int Cost = 1;
	
	[Export]
	public bool OneShot = true;
	
	private Stats _PlayerStats = null;
	
	private PriceTag _priceTag = null;
	private Position2D _itemPosition = null;
	private Loot _item = null;
	
	private bool _empty = true;
	private Player _target = null;

	public override void _Ready()
	{
		_PlayerStats = GetNode("/root/PlayerStats") as Stats;
		
		_priceTag = GetNode("PriceTag") as PriceTag;
		_itemPosition = GetNode("ItemPosition") as Position2D;
		
		if (_itemPosition.GetChildCount() > 0)
		{
			_item = _itemPosition.GetChildren()[0] as Loot;
			_item.Locked = true;
			_priceTag.CostValue = Cost;
			_empty = false;
		}
		
		_priceTag.Visible = false;
		_priceTag.CostValue = Cost;
	}
	
	private void _buy()
	{
		if (_PlayerStats.Has("Coin", Cost))
		{
			_PlayerStats.OffsetInventory("Coin", -Cost);
			Loot newItem = null;
			
			if (OneShot)
			{
				_empty = true;
				_priceTag.Visible = false;
				_target.SetInteractible(false);
			}
			else
			{
				newItem = _item.Duplicate() as Loot;
				newItem.Locked = true;
				_itemPosition.AddChild(newItem);
			}
			
			_item.Unlock();
			
			if (!OneShot)
			{
				_item = newItem;
			}
		}
	}
	
	private void _ignore()
	{
		_target.Disconnect("Ignore", this, "_ignore");
		_target.Disconnect("Interact", this, "_buy");
		_target = null;
		
		_priceTag.Visible = false;
	}

//	SIGNAL
	private void _on_InteractArea_body_entered(object body)
	{
		if (body is Player player && !_empty && _target == null)
		{
			_target = player;
			player.SetInteractible(true);
			player.Connect("Interact", this, "_buy");
			player.Connect("Ignore", this, "_ignore");
			_priceTag.Visible = true;
		}
	}

	private void _on_InteractArea_body_exited(object body)
	{
		if (body is Player player && _target==player)
		{
			player.SetInteractible(false);
			_ignore();
		}
	}
}

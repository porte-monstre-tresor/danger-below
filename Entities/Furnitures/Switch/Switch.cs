using Godot;
using System;

public class Switch : Node2D
{
	[Signal]
	delegate void SwitchTurned(bool val);
	
	[Signal]
	delegate void SwitchOn();
	
	[Signal]
	delegate void SwitchOff();
	
	private Sprite _sprite = null;
	
	private Player _target = null;
	
	private bool _state = false;
	public bool State
	{
		get { return _state; }
		set {
			_setSwitchStatus(value);
		}
	}
	
	public bool Locked = false;

	public override void _Ready()
	{
		_sprite = GetNode("Sprite") as Sprite;
	}
	
	private void _setSwitchStatus(bool val)
	{
		_sprite.Frame = val?1:0;
		_state = val;
	}

	private void _switch()
	{
		if (!Locked)
		{
			State = !State;
			if (State)
			{
				EmitSignal("SwitchTurned", true);
				EmitSignal("SwitchOn");
			}
			else
			{
				EmitSignal("SwitchTurned", false);
				EmitSignal("SwitchOff");
			}
		}
	}
	
	private void _ignore()
	{
		_target.Disconnect("Interact", this, "_switch");
		_target.Disconnect("Ignore", this, "_ignore");
		_target = null;
	}

// ############################################################################
// SIGNALS
	
	private void _on_InteractArea_body_entered(object body)
	{
		if (body is Player player && _target == null)
		{
			_target = player;
			player.SetInteractible(true);
			player.Connect("Interact", this, "_switch");
			player.Connect("Ignore", this, "_ignore");
		}
	}
	
	private void _on_InteractArea_body_exited(object body)
	{
		if (body is Player player && _target==player)
		{
			player.SetInteractible(false);
			_ignore();
		}
	}
}

using Godot;
using System;

public class Door : Node2D
{
	[Export]
	public bool KeyRequired = true;
	
	[Export]
	private bool _open = false;
	public bool Open
	{
		get { return _open; }
		set
		{
			if (value)
			{
				_animationPlayer.Queue("OpenDoor");
			}
			else
			{
				_animationPlayer.Queue("CloseDoor");
			}
			_open = value;
		}
	}
	
	private Stats _PlayerStats = null;
	
	private Sprite _closedDoorSprite = null;
	private Sprite _openDoorSprite = null;
	private CollisionShape2D _closedDoorCollider = null;
	private InfoBubble _infoBubble = null;
	private AnimationPlayer _animationPlayer = null;
	
	private Player _target = null;
	
	public override void _Ready()
	{
		_PlayerStats = GetNode("/root/PlayerStats") as Stats;
		
		_closedDoorSprite = GetNode("ClosedDoorSprite") as Sprite;
		_openDoorSprite = GetNode("OpenDoorSprite") as Sprite;
		_closedDoorCollider = GetNode("ClosedDoorCollider") as CollisionShape2D;
		_infoBubble = GetNode("InfoBubble") as InfoBubble;
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
//
		_closedDoorSprite.Visible = !Open;
		_openDoorSprite.Visible = Open;
		_closedDoorCollider.Disabled = Open;
	}
	
	private void _unlock()
	{
		if (_PlayerStats.Has("SilverKey", 1))
		{
			_PlayerStats.OffsetInventory("SilverKey", -1);
			_animationPlayer.Play("Unlock");
			Open = true;
			
			_ignore();
		}
	}
	
	private void _ignore()
	{
		_infoBubble.Hide();
		_target.Disconnect("Ignore", this, "_ignore");
		_target.Disconnect("Interact", this, "_unlock");
		_target = null;
	}
	
// ############################################################################
// SIGNALS
	
	private void _on_InteractArea_body_entered(object body)
	{
		if (body is Player player && !_open && _target == null)
		{
			if (KeyRequired)
			{
				_infoBubble.Display();
				_target = player;
				player.SetInteractible(true);
				player.Connect("Ignore", this, "_ignore");
				player.Connect("Interact", this, "_unlock");
			}
		}
	}
	
	private void _on_InteractArea_body_exited(object body)
	{
		if (body is Player player && _target==player)
		{
			if (KeyRequired)
			{
				_ignore();
				player.SetInteractible(false);
			}
		}
	}
}

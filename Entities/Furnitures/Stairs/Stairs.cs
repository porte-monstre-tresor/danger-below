using Godot;
using System;

public class Stairs : Node2D
{
	private LevelManager _LevelManager = null;
	
	public bool Locked = false;
	
	public override void _Ready()
	{
		_LevelManager = GetNode("/root/LevelManager") as LevelManager;
	}
	
	private void _on_DetectionArea_body_entered(object body)
	{
		if (body is Player player && !Locked)
		{
			player.EraseFromGame();
			_LevelManager.LevelCleared();
		}
	}
}


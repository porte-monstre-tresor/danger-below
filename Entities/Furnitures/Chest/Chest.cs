using Godot;
using System;

public class Chest : StaticBody2D
{
	private Stats _PlayerStats = null;
	private Looter _Looter = null;
	
	private Stats _stats = null;
	private InfoBubble _infoBubble = null;
	private AnimationPlayer _animationPlayer = null;
	private Sprite _sprite = null;
	
	[Export]
	public String Content = "Coin";
	
	[Export]
	public int Amount = 15;
	
	private Player _target = null;
	private bool _open;
	
	public override void _Ready()
	{
		_PlayerStats = GetNode("/root/PlayerStats") as Stats;
		_Looter = GetNode("/root/Looter") as Looter;
		
		_stats = GetNode("Stats") as Stats;
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
		_infoBubble = GetNode("InfoBubble") as InfoBubble;
		_sprite = GetNode("Sprite") as Sprite;
		
		_sprite.Frame = 0;
		
		_stats.EditInventory(Content, Amount);
	}

	public void OpenChest()
	{
		_animationPlayer.Play("Open");
		_open = true;
	}
	
	private void _unlock()
	{
		if (_PlayerStats.Has("GoldenKey", 1))
		{
			_PlayerStats.OffsetInventory("GoldenKey", -1);
			OpenChest();
			
			_target.SetInteractible(false);
		}
	}
	
	private void _ignore()
	{
		_infoBubble.Hide();
		_target.Disconnect("Ignore", this, "_ignore");
		_target.Disconnect("Interact", this, "_unlock");
		_target = null;
	}
	
	private void _dropContent()
	{
		_stats.Health = 0;
	}
	
	private void _on_InteractArea_body_entered(object body)
	{
		if (body is Player player && !_open && _target == null)
		{
			_target = player;
			player.SetInteractible(true);
			player.Connect("Interact", this, "_unlock");
			player.Connect("Ignore", this, "_ignore");
			_infoBubble.Display();
		}
	}

	private void _on_InteractArea_body_exited(object body)
	{
		if (body is Player player && _target==player)
		{
			player.SetInteractible(false);
			_ignore();
		}
	}

	private void _on_Stats_NoHealth(Godot.Collections.Array<String> drops)
	{
		_Looter.DropItemAt(drops, GetParent().GetPath(), GlobalPosition);
	}
}


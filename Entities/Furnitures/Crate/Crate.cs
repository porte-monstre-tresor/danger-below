using Godot;
using System;

public class Crate : StaticBody2D
{
	[Export(PropertyHint.Enum, "Random, Fix")]
	public int Type = 0;
	
	[Export]
	public String Content = "Coin";
	
	[Export]
	public int Amount = 5;
	
	private Looter _Looter = null;
	private Weapons _Weapons = null;
	private AnimationPlayer _animationPlayer = null;
	
	private Stats _stats = null;
	
	public override void _Ready()
	{
		_Looter = GetNode("/root/Looter") as Looter;
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
		_Weapons = GetNode("/root/Weapons") as Weapons;
		
		_stats = GetNode("Stats") as Stats;
		
		switch(Type)
		{
			case 1:
				_stats.EditInventory(Content, Amount);
				break;
			case 0:
				var rng = new RandomNumberGenerator();
				rng.Randomize();
				var randomLoot = rng.Randf();
				
//				Weapon
				if (randomLoot < 0.05f)
				{
					_stats.EditInventory(_Weapons.WeaponsList[(int)(rng.Randi() % _Weapons.WeaponsList.Count)], 1);
				}
				
//				Heart
				else if (randomLoot < 0.10f)
				{
					_stats.EditInventory("Heart", 1);
				}

//				Key
				else if (randomLoot < 0.20f)
				{
					_stats.EditInventory("SilverKey", 1);
				}
				else if (randomLoot < 0.25f)
				{
					_stats.EditInventory("GoldenKey", 1);
				}
				
//				Coin
				else if (randomLoot < 0.75f)
				{
					_stats.EditInventory("Coin", (int)(rng.Randi()%2)+1);
				}
				break;
		}
	}

	private void _on_Hurtbox_area_entered(object area)
	{
		if (area is Hitbox hitbox)
		{
			_stats.Health -= hitbox.Damage;
		}
	}

	private void _on_Stats_NoHealth(Godot.Collections.Array<String> drops)
	{
		_Looter.DropItemAt(drops, GetParent().GetPath(), GlobalPosition);
		_animationPlayer.Play("Break");
	}
}



using Godot;
using System;

public class Goblin : Enemy 
{
	private float _oneCoinThreshold = 0.3f;
	private int _coins = 0;
	private DeathEffect _deathEffect = null;
	
	protected override void _setupEnemy()
	{
		_deathEffect = GetNode("DeathEffect") as DeathEffect;
		
		var stat = GD.Randf();
		
		if (stat<_oneCoinThreshold)
		{
			_coins = 1;
		}
		_stats.EditInventory("Coin", _coins);
	}
	
	protected override void _onDeath()
	{
		_deathEffect.CloneInCurrentScene(_lastHitAngle);
	}
}

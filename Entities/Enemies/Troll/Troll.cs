using Godot;
using System;

public class Troll : Enemy
{
	private Position2D _weaponRotater = null;
	private NailedBat _nailedBat = null;
	private AnimationPlayer _attackAnimationPlayer = null;
	private DeathEffect _deathEffect = null;
	
	protected override void _setupEnemy()
	{
		_weaponRotater = GetNode("WeaponRotater") as Position2D;
		_nailedBat = GetNode("WeaponRotater/WeaponHandle/NailedBat") as NailedBat;
		_attackAnimationPlayer = GetNode("AttackAnimationPlayer") as AnimationPlayer;
		_deathEffect = GetNode("DeathEffect") as DeathEffect;
	}
	
	protected override void _processEnemy(float delta)
	{
		_weaponRotater.Rotation = _direction.Angle();
	}
	
	private void _useBat()
	{
		_nailedBat.Attack();
	}
	
	protected override void _onDeath()
	{
		_deathEffect.CloneInCurrentScene(_lastHitAngle);
	}

	private void _on_AttackTimer_timeout()
	{
		if (_playerDetectionRay.IsTargetPlayer && _state == States.Chase)
		{
			_attackAnimationPlayer.Play("Attack");
		}
	}
}


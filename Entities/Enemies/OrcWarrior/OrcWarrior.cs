using Godot;
using System;

public class OrcWarrior : Enemy 
{
	private Position2D _weaponRotater = null;
	private Spear _spear = null;
	private DeathEffect _deathEffect = null;
	
	protected override void _setupEnemy()
	{
		_weaponRotater = GetNode("WeaponRotater") as Position2D;
		_spear = GetNode("WeaponRotater/WeaponHandle/Spear") as Spear;
		_deathEffect = GetNode("DeathEffect") as DeathEffect;
		
//		Loots:
		var coins = 0;
		coins = (GD.Randf()<0.5f) ? 1 : coins;
		coins = (GD.Randf()<0.1f) ? 2 : coins;
		_stats.EditInventory("Coin", coins);
		
		var spear = (GD.Randf()<0.1f) ? 1 : 0;
		_stats.EditInventory("Spear", spear);
	}
	
	protected override void _processEnemy(float delta)
	{
		_weaponRotater.Rotation = _direction.Angle();
	}
	
	protected override void _onDeath()
	{
		_deathEffect.CloneInCurrentScene(_lastHitAngle);
	}

	private void _on_AttackTimer_timeout()
	{
		if (_playerDetectionRay.IsTargetPlayer && _state == States.Chase)
		{
			_spear.Attack();
		}
	}
}


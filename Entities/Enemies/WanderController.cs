using Godot;
using System;

public class WanderController : Node2D
{
	[Export]
//	Default max range where the character could wander
	private float _wanderRange = 32;
	
//	Where the character spawned
	private Vector2 _startPosition;
	
//	Where the charcater will go when it will wander
	public Vector2 TargetPosition;
	
//	Children nodes used
	private Timer _timer = null;
	
	public override void _Ready()
	{
//		Get children nodes used
		_timer = GetNode("Timer") as Timer;
		
//		Initialize the starting point and the first target as itself
		_startPosition = GlobalPosition;
		TargetPosition = GlobalPosition;
		
//		Set the first wander target
		_updateTargetPosition();
	}
	
	private void _updateTargetPosition()
	{
//		Place a target somewhere random in the wandering range
		var targetVector = new Vector2((float)GD.RandRange(-_wanderRange, _wanderRange), (float)GD.RandRange(-_wanderRange, _wanderRange));
		TargetPosition = _startPosition + targetVector;
	}
	
	public float GetTimeLeft()
	{
		return _timer.TimeLeft;
	}
	
	public void StartWanderTimer(float duration)
	{
		_timer.Start(duration);
	}
	
	private void _onTimerTimeout()
	{
//		Wander somewhere else in a regular basis
		_updateTargetPosition();
	}	
}



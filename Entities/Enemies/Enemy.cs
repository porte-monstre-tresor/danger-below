using Godot;
using System;
using System.Collections.Generic;

public class Enemy : KinematicBody2D
{
	[Signal]
	delegate void EnemyDeath();
	
//	STATE MACHINE
	protected enum States {
		Idle,	// The enemy doesn't move
		Wander,	// The enemy wanders around its origin point
		Chase	// The enemy chases the player
	}
	protected States _state = States.Idle;
//	#############
	
//	MOVEMENT VARS:
//	Deceleration
	[Export]
	protected int _friction = 200;
	
//	Acceleration
	[Export]
	protected int _acceleration = 300;
	
//	Max speed
	[Export]
	protected int _maxSpeed = 50;
	
//	Divide the knowback
	[Export]
	protected int _weight = 5;
	
//	Range of wander
	[Export]
	protected int _wanderRange = 4;
// ##############

//	Private vars
	private Vector2 _velocity = Vector2.Zero;
	private Vector2 _knockback = Vector2.Zero;
	protected Vector2 _direction = Vector2.Zero;
	protected float _lastHitAngle = 0f;
	
//	Children nodes used
	private Sprite _sprite = null;
	private AnimationPlayer _animationPlayer = null;
	private AnimationPlayer _blinkAnimationPlayer = null;
	protected Stats _stats = null;
	protected PlayerDetectionRay _playerDetectionRay = null;
	private SoftCollision _softCollision = null;
	private WanderController _wanderController = null;
	
	private Looter _Looter = null;
	
	public override void _Ready()
	{
//		Different initial state depending on the runs
		GD.Randomize();
		
//		Get the specified children nodes (since onready doesn't work yet)
		_sprite = GetNode("Sprite") as Sprite;
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
		_blinkAnimationPlayer = GetNode("BlinkAnimationPlayer") as AnimationPlayer;
		_stats = GetNode("Stats") as Stats;
		_playerDetectionRay = GetNode("PlayerDetectionRay") as PlayerDetectionRay;
		_softCollision = GetNode("SoftCollision") as SoftCollision;
		_wanderController = GetNode("WanderController") as WanderController;
		
		_Looter = GetNode("/root/Looter") as Looter;
		
//		Random initial state to start
		_state = States.Idle;
		
		_animationPlayer.Play("IdleDown");
		_setupEnemy();
	}
	
	protected virtual void _setupEnemy(){}

	public override void _PhysicsProcess(float delta)
	{
//		Impact of the potential knockback
		_knockback = _knockback.MoveToward(Vector2.Zero, _friction * delta);
		_knockback = MoveAndSlide(_knockback);
		
//		State machine
		switch(_state)
		{
//			The enemy stays still
			case States.Idle:
				_velocity = _velocity.MoveToward(Vector2.Zero, _friction * delta);

				if (_velocity.y < 0)
					_animationPlayer.Play("IdleUp");
				else if (_velocity.y > 0)
					_animationPlayer.Play("IdleDown");

//				Random change between Idle and Wander
				if ( _wanderController.GetTimeLeft() == 0.0f )
				{
					_state = _pickRandomState();
					_wanderController.StartWanderTimer((float)GD.RandRange(1f, 3f));
				}
				break;

//			The enemy wanders around a bit
			case States.Wander:
				_direction = GlobalPosition.DirectionTo(_wanderController.TargetPosition);
				_playerDetectionRay.DirectTowards(_direction);

				_velocity = _velocity.MoveToward(_direction * _maxSpeed, _acceleration * delta);

				if (_velocity.y < 0)
					_animationPlayer.Play("RunUp");
				else
					_animationPlayer.Play("RunDown");
				_sprite.FlipH = _velocity.x < 0; 

//				Random change between Idle and Wander/wander goal reached
				if ((_wanderController.GetTimeLeft()==0.0f) || (GlobalPosition.DistanceTo(_wanderController.TargetPosition)<=_wanderRange))
				{
					_state = _pickRandomState();
					_wanderController.StartWanderTimer((float)GD.RandRange(1f, 3f));
				}
				break;

//			The enemy chase the player/its fart
			case States.Chase:
				_direction = GlobalPosition.DirectionTo(_playerDetectionRay.Target);
				_playerDetectionRay.DirectTowards(_direction);

				_velocity = _velocity.MoveToward(_direction * _maxSpeed, _acceleration * delta);

				if (_velocity.y < 0)
					_animationPlayer.Play("RunUp");
				else
					_animationPlayer.Play("RunDown");
				_sprite.FlipH = _velocity.x < 0; 
				break;
		}
//		Resolve the soft collisions
		if (_softCollision.IsColliding())
		{
			_velocity += _softCollision.GetPushVector() * delta * 400;
		}
//		Resolve the movement
		_velocity = MoveAndSlide(_velocity);
		_processEnemy(delta);
	}
	
	protected virtual void _processEnemy(float delta) {}
	
	private States _pickRandomState()
	{
		var stateList = new List<States>{States.Idle, States.Wander};
		var i = GD.Randi() % stateList.Count;
		return stateList[(int)i];
	}

//	SIGNAL
	private void _onHurtboxAreaEntered(object area)
	{
		if (area is Hitbox hitbox)
		{
			_playerDetectionRay.DirectTowards(GlobalPosition.DirectionTo(hitbox.GlobalPosition));
			
			var direction = hitbox.GlobalPosition.DirectionTo(GlobalPosition);
			_knockback = hitbox.KnockbackStrength * direction / _weight;
			_stats.Health -= hitbox.Damage;
			_lastHitAngle = GlobalPosition.AngleToPoint(hitbox.GlobalPosition);
			_blinkAnimationPlayer.Play("Hurt");
		}
	}
	
	private void _onStatsNoHealth(Godot.Collections.Array<String> drops)
	{
		EmitSignal("EnemyDeath");
		_onDeath();
		_Looter.DropItemAt(drops, GetParent().GetPath(), GlobalPosition);
		QueueFree();
	}
	
	protected virtual void _onDeath() {}

	private void _on_PlayerDetectionRay_TargetAcquired()
	{
		_state = States.Chase;
	}

	private void _on_PlayerDetectionRay_TargetLost()
	{
		_state = States.Idle;
	}
}


using Godot;
using System;

// Script shared by the enemies as their stats and by the player as a singleton

public class Stats : Node
{
	[Signal]
	delegate void DisplayGameOver();
	
	[Signal]
	delegate void NoHealth(Godot.Collections.Array<String> drops);
	
	[Signal]
	delegate void HealthChanged(int health);
	
	[Signal]
	delegate void MaxHealthChanged(int maxHealth);
	
	[Signal]
	delegate void SetWalletUI(String item, int amount);
	
	[Export]
	public int _MaxHealth = 1;
	public int MaxHealth
	{
		get { return _MaxHealth; }
		set 
		{
			_MaxHealth = value;
			this.Health = Math.Min(Health, _MaxHealth);
			EmitSignal("MaxHealthChanged", _MaxHealth);
		}
	}
	
	public int _Health;
	public int Health 
	{ 
		get { return _Health; }
		set
		{
			_Health = Mathf.Clamp(value, 0, MaxHealth);
			EmitSignal("HealthChanged", _Health);
			if (_Health <= 0)
			{
				int i;
				var drops = new Godot.Collections.Array();
				foreach (String key in _inventory.Keys)
				{
					for (i=0; i<(int)_inventory[key]; i++)
					{
						drops.Add(key);
					}
				}
				EmitSignal("NoHealth", drops);
			}
		} 
	}
	
	private Godot.Collections.Dictionary _inventory = new Godot.Collections.Dictionary();
	
//	Used to init the inventory (ie: setup the loot of a monster in editor)
	[Export]
	protected int _silverKeys = 0;
	
	[Export]
	protected int _goldenKeys = 0;
	
	[Export]
	protected int _coins = 0;
	
	[Export]
	protected int _hearts = 0;
	
	[Export]
	protected int _axes = 0;
	
	[Export]
	protected int _knives = 0;
	
	[Export]
	protected int _regularSwords = 0;
	
	[Export]
	protected int _spears = 0;
	
	[Export]
	protected int _nailedBats = 0;
	
	public override void _Ready()
	{
		Health = MaxHealth;
		
		_inventory["SilverKey"] = _silverKeys;
		_inventory["GoldenKey"] = _goldenKeys;
		_inventory["Coin"] = _coins;
		_inventory["Heart"] = _hearts;
		_inventory["Axe"] = _axes;
		_inventory["Knife"] = _knives;
		_inventory["Regular Sword"] = _regularSwords;
		_inventory["Spear"] = _spears;
		_inventory["Nailed Bat"] = _nailedBats;
	}
	
	public void ResetCurrencies()
	{
		foreach (String key in _inventory.Keys)
		{
			_inventory[key] = 0;
		}
	}
	
	public bool Has(String currency, int amount = 1)
	{
		return ((int)_inventory[currency] >= amount);
	}
	
	public void EditInventory(String item, int amount)
	{
		_inventory[item] = amount;
		EmitSignal("SetWalletUI", item, amount);
	}
	
	public void OffsetInventory(String item, int offset)
	{
		_inventory[item] = (int)_inventory[item] + offset;
		EmitSignal("SetWalletUI", item, _inventory[item]);
	}
	
	public void UpdateWalletUI()
	{
		EmitSignal("SetWalletUI", "SilverKey", _inventory["SilverKey"]);
		EmitSignal("SetWalletUI", "GoldenKey", _inventory["GoldenKey"]);
		EmitSignal("SetWalletUI", "Coin", _inventory["Coin"]);
	}

	private void _on_PlayerStats_NoHealth(Godot.Collections.Array<int> drops)
	{
		EmitSignal("DisplayGameOver");
	}
}


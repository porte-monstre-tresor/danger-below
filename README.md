# Danger Below

You're a hero in a dungeon, trying to defeat the Four Horsemen of the Apocalypse, with a bad but really quick-working blacksmith. Use a huge pile of single-use weapons to take down hords of monsters ! 

This is an iterative project to learn more about game-making and all of its components. 

## How to use

You can get the game [on my Itch.io page](https://canopteks.itch.io/dangerbelow), get it from this repository as pipeline artifacts or build it yourself with the editor Godot 3.3.2 (Mono version).

This game is a Work In Progress and I'm actively working on it, so stay tuned for more content soon !

## Acknowledgments

The [Godot Editor](https://godotengine.org/) is the wonderful tool, free and open-source, that I use in this project. I'm using the Mono-version so I can code in C#.

I'm also using:
- [Dungeon Tileset II](https://0x72.itch.io/dungeontileset-ii) as graphic base, by Ox72 and GrafxKid,
- [Dungeon UI](https://0x72.itch.io/dungeonui) as UI parts, by 0x72,
- [Dungeon Font](https://vrtxrry.itch.io/dungeonfont) by vrtxrry and Ox72,
- [Zapsplat](https://www.zapsplat.com) and [BigSoundBank](https://bigsoundbank.com/) for sound effects,
- [Nctrnm](https://nctrnm.bandcamp.com/) and its track Dos Lagos as soundtrack. 

A big thank you to each and every of the creators who allow people like me to use their work in our creations by placing it under free/extra-permisive licenses (like CC0 or Attribution required).

I use the GitHub project [Godot CI](https://github.com/abarichello/godot-ci), it is really helpful.

## License

This project is shared under [WTFPL](LICENSE.txt), which means you can use it for whatever and however you want, and credit is not necessary.

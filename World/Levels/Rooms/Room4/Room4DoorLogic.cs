using Godot;
using System;

public class Room4DoorLogic : YSort
{
	private Door _doorCenter = null;
	private Door _doorRight = null;
	private Door _doorLeft = null;
	private Switch _switchDown = null;
	private Switch _switchUp = null;
	
	public override void _Ready()
	{
		_doorCenter = GetNode("DoorCenter") as Door;
		_doorRight = GetNode("DoorRight") as Door;
		_doorLeft = GetNode("DoorLeft") as Door;
		_switchDown = GetNode("SwitchDown") as Switch;
		_switchUp = GetNode("SwitchUp") as Switch;
		
		_switchDown.Connect("SwitchTurned", this, "_on_SwitchTurned");
		_switchUp.Connect("SwitchTurned", this, "_on_SwitchTurned");
	}

	private void _on_SwitchTurned(bool val)
	{
		_doorCenter.Open = !_doorCenter.Open;
		_doorRight.Open = !_doorRight.Open;
		_doorLeft.Open = !_doorLeft.Open;
	}
}

using Godot;
using System;

public class Room2DoorLogic : YSort
{
	private Door _door = null;
	private Switch _switchLeft = null;
	private Switch _switchRight = null;
	
	public override void _Ready()
	{
		_door = GetNode("Door") as Door;
		_switchLeft = GetNode("SwitchLeft") as Switch;
		_switchRight = GetNode("SwitchRight") as Switch;
		
		_switchLeft.Connect("SwitchTurned", this, "_on_SwitchTurned");
		_switchRight.Connect("SwitchTurned", this, "_on_SwitchTurned");
	}

	private void _on_SwitchTurned(bool val)
	{
		_door.Open = !_door.Open;
	}
}

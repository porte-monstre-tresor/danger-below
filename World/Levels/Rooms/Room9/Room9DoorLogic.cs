using Godot;
using System;

public class Room9DoorLogic : YSort
{
	private Switch _switch = null;
	private Door _door = null;
	
	public override void _Ready()
	{
		_switch = GetNode("Switch") as Switch;
		_door = GetNode("Door") as Door;
		
		_switch.Connect("SwitchOn", this, "_on_Switch_SwitchOn");
	}

	private void _on_Switch_SwitchOn()
	{
		_door.Open = true;
	}
}

using Godot;
using System;

public class Lobby : Node2D
{
	private LevelManager _LevelManager = null;

	public override void _Ready()
	{
		_LevelManager = GetNode ("/root/LevelManager") as LevelManager;
	}

	private void _on_TutorialExit_ExitTaken()
	{
		_LevelManager.StartTutorial();
	}

	private void _on_OrcExit_ExitTaken()
	{
		_LevelManager.StartRun();
	}
}


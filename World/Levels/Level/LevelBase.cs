using Godot;
using System;
using Godot.Collections;

public class LevelBase : Node2D
{
	protected enum RoomType
	{
		None,
		Block,
		Entry,
		Random,
		Exit
	}
	
	public static int Size = 3;
	public static int Diversity = 1;
	
	protected RoomType[,] _grid;
	
	protected Vector2 _chunkSize = new Vector2(16, 16);
	protected Vector2 _tileSize = new Vector2(16, 16);
	
	protected TileMap _floorTileMap = null;
	protected TileMap _wallTileMap = null;
	protected Player _player = null;
	
	private RandomNumberGenerator _rng = new RandomNumberGenerator();
	
	protected Array<String> ROOMS_PATH = new Array<String>{
		"res://World/Levels/Rooms/Room0/Room0.tscn", // BLOCK ROOM
		"res://World/Levels/Rooms/Room1/Room1.tscn", // SPAWN ROOM
		"res://World/Levels/Rooms/Room2/Room2.tscn", // REGULAR ROOM
		"res://World/Levels/Rooms/Room3/Room3.tscn", // same for the others
		"res://World/Levels/Rooms/Room4/Room4.tscn",
		"res://World/Levels/Rooms/Room5/Room5.tscn",
		"res://World/Levels/Rooms/Room6/Room6.tscn",
		"res://World/Levels/Rooms/Room7/Room7.tscn",
		"res://World/Levels/Rooms/Room8/Room8.tscn",
		"res://World/Levels/Rooms/Room9/Room9.tscn",
		"res://World/Levels/Rooms/Room10/Room10.tscn",
		"res://World/Levels/Rooms/Room11/Room11.tscn"
	};
	
	public override void _Ready()
	{
		if (Size%2==0)
		{
			GD.Print("Warning: size level is supposed to be uneven");
		}
		
		_floorTileMap = GetNode("FloorTileMap") as TileMap;
		_wallTileMap = GetNode("WallTileMap") as TileMap;
		_player = GetNode("WallTileMap/Player") as Player;
		
		_rng.Randomize();
		
//		Init the grid for the level generation
		_grid = new RoomType[Size+2, Size+2];
		for (int i = 0; i < Size+2; i++)
		{
			for (int j = 0; j < Size+2; j++)
			{
				if (i==0 || i==(Size+1) || j==0 || j==(Size+1) || (i%2==0 && j%2==0))
				{
					_grid[i, j] = RoomType.Block;
				}
				else
				{
					_grid[i, j] = RoomType.None;
				}
			}
		}
		
//		Select a seed origin in the available chunks
		Vector2 seedOrigin;
		seedOrigin.x = (_rng.Randi() % Size) + 1;
		if (seedOrigin.x%2 == 0)
		{
			seedOrigin.y = (_rng.Randi() % ((Size+1)/2)) * 2 + 1;
		}
		else
		{
			seedOrigin.y = (_rng.Randi() % Size) + 1;
		}
		_grid[(int)seedOrigin.x, (int)seedOrigin.y] = RoomType.Entry;
		
//		Compute a level with a seed algorithm
		Godot.Collections.Array currentTree = new Godot.Collections.Array{seedOrigin};
		Godot.Collections.Array ngbr;
		for (int i = 0; i<(Size-1)*(Size-1); i++)
		{
			ngbr = _getNeighbors(currentTree, RoomType.None);
			Vector2 newElt = (Vector2)ngbr[(int)(_rng.Randi() % ngbr.Count)];
			_grid[(int)newElt.x, (int)newElt.y] = RoomType.Random;
			currentTree.Add(newElt);
		}
		
//		Clean the grid and prepare the search of the furthest point
		int[,] floodFillGrid = new int[Size+2, Size+2];
		for (int i = 0; i < Size+2; i++)
		{
			for (int j = 0; j < Size+2; j++)
			{
				if (_grid[i, j] == RoomType.None)
				{
					_grid[i, j] = RoomType.Block;
				}
				
				if (_grid[i, j] == RoomType.Random)
				{
					floodFillGrid[i, j] = 0;
				}
				else
				{
					floodFillGrid[i, j] = -1;
				}
			}
		}
		
//		Flood-fill algo on the path created
		currentTree = new Godot.Collections.Array{seedOrigin};
		ngbr = _getNeighbors(currentTree, RoomType.Random);
		int delta = 1;
		while (delta>0)
		{
			foreach (Vector2 pos in ngbr)
			{
				delta = 0;
				int x = (int)pos.x;
				int y = (int)pos.y;
				if (floodFillGrid[x, y] == 0)
				{
					delta += 1;
					floodFillGrid[x, y] = 1;
					currentTree.Add(new Vector2(x, y));
				} 
			}
			
			ngbr = _getNeighbors(currentTree, RoomType.Random);
		}
		
//		Last point captured by the flood-fill is the furthest
		Vector2 exitPos = (Vector2)currentTree[currentTree.Count-1];
		_grid[(int)exitPos.x, (int)exitPos.y] = RoomType.Exit;
		
		for (int i = 0; i < Size+2; i++)
		{
			for (int j = 0; j < Size+2; j++)
			{
				int x = i*(int)_chunkSize.x;
				int y = j*(int)_chunkSize.y;
				
				if (_grid[i, j] == RoomType.Block)
				{
					_createRoom(ROOMS_PATH[0], x, y);
				}
				else if (_grid[i, j] == RoomType.Entry)
				{
					_createRoom(ROOMS_PATH[1], x, y, false, true);
				}
				else if (_grid[i, j] == RoomType.Exit)
				{
					int id = (int)(_rng.Randi() % (ROOMS_PATH.Count-2)) + 2;
					_createRoom(ROOMS_PATH[id], x, y, true, false);
				}
				else if (_grid[i, j] == RoomType.Random)
				{
					int id = (int)(_rng.Randi() % (ROOMS_PATH.Count-2)) + 2;
					_createRoom(ROOMS_PATH[id], x, y);
				}
				else
				{
					GD.Print("Error: unknown room loaded");
				}
			}
		}
	}
	
//	!!! This function has very few sanity checks because the seed can't be on a border of the grid 
	private Godot.Collections.Array _getNeighbors(Godot.Collections.Array currentTree, RoomType ngbrType)
	{
		var ngbr = new Godot.Collections.Array();
		
		foreach (Vector2 pos in currentTree)
		{
			int x = (int)pos.x;
			int y = (int)pos.y;
			
			if (_grid[x+1, y] == ngbrType)// && !currentTree.Contains(new Vector2(x+1, y)))
			{
				ngbr.Add(new Vector2(x+1, y));
			}
			if (_grid[x-1, y] == ngbrType)// && !currentTree.Contains(new Vector2(x-1, y)))
			{
				ngbr.Add(new Vector2(x-1, y));
			}
			if (_grid[x, y+1] == ngbrType)// && !currentTree.Contains(new Vector2(x, y+1)))
			{
				ngbr.Add(new Vector2(x, y+1));
			}
			if (_grid[x, y-1] == ngbrType)// && !currentTree.Contains(new Vector2(x, y-1)))
			{
				ngbr.Add(new Vector2(x, y-1));
			}
		}
		
		return ngbr;
	}
	
	private void _createRoom(String path, int x, int y, bool isExit=false, bool isEntry=false)
	{
		var roomScene = GD.Load(path) as PackedScene;
		var room = roomScene.Instance() as Node2D;
		
//		Tilemaps copy in the main one
		var roomWallTileMap = room.GetNode("WallTileMap") as TileMap;
		var roomFloorTileMap = room.GetNode("FloorTileMap") as TileMap;
		for (int i = 0; i < _chunkSize.x; i++)
		{
			for (int j = 0; j < _chunkSize.y; j++)
			{
				_wallTileMap.SetCell(x+i, y+j, roomWallTileMap.GetCell(i, j));
				
				_floorTileMap.SetCell(x+i, y+j, roomFloorTileMap.GetCell(i, j));
			}
		}
		
		_wallTileMap.UpdateBitmaskRegion();
		_floorTileMap.UpdateBitmaskRegion();
		
		roomWallTileMap.QueueFree();
		roomFloorTileMap.QueueFree();
		
//		Content copy
		var roomPos = new Vector2(x*_chunkSize.x, y*_chunkSize.y);
		var childCount = 0;
		
//		Always display the absolute content
		var absoluteContent = room.GetNode("AbsoluteContent") as YSort;
		if (absoluteContent.GetChildCount() == 0)
		{
			absoluteContent.QueueFree();
		}
		else
		{
			absoluteContent.RemoveAndSkip();
		}
		
//		Remove the exit if the room is not the exit, make sure it's visible if it is
		var exit = room.GetNode("ExitContent") as YSort;
		if (!isExit)
		{
			exit.QueueFree();
		}
		else
		{
			exit.RemoveAndSkip();
		}
		
		if (isEntry)
		{
			var playerPosition = room.GetNode("PlayerPosition") as Position2D;
			_player.GlobalPosition = roomPos + playerPosition.Position;
		}
		
//		Display one of the relative contents and remove the others
		var relativeId = (_rng.Randi()%Diversity);
		for (int i = 0; i < 5; i++)
		{
			if (room.HasNode(new NodePath("RelativeContent"+i)))
			{
				var relativeContent = room.GetNode("RelativeContent"+i) as YSort;
				if (i != relativeId || relativeContent.GetChildCount() == 0)
				{
					relativeContent.QueueFree();
				}
				else
				{
					relativeContent.RemoveAndSkip();
				}
			}
		}
		
//		Copy the prepared room
		room.GlobalPosition = roomPos;
		_wallTileMap.AddChild(room);
		foreach (Node2D child in room.GetChildren())
		{
			child.GlobalPosition += roomPos;
		}
		room.RemoveAndSkip();
	}
}

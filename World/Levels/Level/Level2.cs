using Godot;
using System;

public class Level2 : Node2D
{
	private Door _entryDoor = null;
	private Door _exitDoor = null;
	
	public override void _Ready()
	{
		_entryDoor = GetNode("WallTileMap/EntryDoor") as Door;
		_exitDoor = GetNode("WallTileMap/ExitDoor") as Door;
	}

	private void _on_Switch_SwitchOn()
	{
		_entryDoor.SetDeferred("Open", true);
	}

	private void _on_EntryDoorClosingTrigger_body_entered(object body)
	{
		if (body is Player)
		{
			_entryDoor.SetDeferred("Open", false);
		}
	}
	
	private void _on_Troll_EnemyDeath()
	{
		_exitDoor.SetDeferred("Open", true);
	}
}





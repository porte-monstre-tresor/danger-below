using Godot;
using System;

public class Tutorial : Node2D
{
	private Keybinds _Keybinds = null;
	private LevelManager _LevelManager = null;
	
	private HintUI _hintUI = null;
	
	private Death _deathTutoKey = null;
	private SilverKey _silverKey = null;
	
	private Death _deathTutoSwitch = null;
	private Switch _switchLeft = null;
	private DestructionEffect _switchLeftSmoke = null;
	private Switch _switchRight = null;
	private DestructionEffect _switchRightSmoke = null;
	private Door _doorTutoSwitch = null;
	
	private Death _deathTutoEnemy = null;
	private Door _doorTutoEnemy = null;
	private int _skeletonCounter = 3;
	
	private Death _deathTutoEnd = null;
	private Door _doorTutoEnd = null;
	private Player _player = null;
	
	private bool _tutoKey = false;
	private bool _tutoSwitch = false;
	private bool _tutoEnemy = false;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_Keybinds = GetNode("/root/Keybinds") as Keybinds;
		_LevelManager = GetNode("/root/LevelManager") as LevelManager;
		
		_hintUI = GetNode("HUD/HintUI") as HintUI;
		var interactScancode = Convert.ToUInt32(_Keybinds.Bindings["interact"]);
		_hintUI.Text = "Press " + OS.GetScancodeString(interactScancode) + " to interact."; 
	
//		Tuto key
		_deathTutoKey = GetNode("WallTileMap/DeathTutoKey") as Death;
		_deathTutoKey.DialogFile = "res://Entities/Npcs/Death/death_tuto_key.json";
		
		_silverKey = GetNode("WallTileMap/SilverKey") as SilverKey;
		_silverKey.Locked = true;
		_silverKey.Visible = false;
		
//		Tuto switch
		_deathTutoSwitch = GetNode("WallTileMap/DeathTutoSwitch") as Death;
		_deathTutoSwitch.DialogFile = "res://Entities/Npcs/Death/death_tuto_switch.json";
		_deathTutoSwitch.Visible = false;
		
		_switchLeft = GetNode("WallTileMap/SwitchLeft") as Switch;
		_switchLeft.Locked = true;
		_switchLeft.Visible = false;
		_switchLeftSmoke = GetNode("WallTileMap/SwitchLeftSmoke") as DestructionEffect;
		
		_switchRight = GetNode("WallTileMap/SwitchRight") as Switch;
		_switchRight.Locked = true;
		_switchRight.Visible = false;
		_switchRightSmoke = GetNode("WallTileMap/SwitchRightSmoke") as DestructionEffect;
		
		_doorTutoSwitch = GetNode("WallTileMap/DoorSwitch") as Door;
		
//		Tuto enemy
		_deathTutoEnemy = GetNode("WallTileMap/DeathTutoEnemy") as Death;
		_deathTutoEnemy.DialogFile = "res://Entities/Npcs/Death/death_tuto_enemy.json";
		_deathTutoEnemy.Visible = false;
		
		_doorTutoEnemy = GetNode("WallTileMap/DoorEnemy") as Door;
		
//		Tuto end
		_deathTutoEnd = GetNode("WallTileMap/DeathTutoEnd") as Death;
		_deathTutoEnd.DialogFile = "res://Entities/Npcs/Death/death_tuto_end.json";
		_deathTutoEnd.Visible = false;
		
		_doorTutoEnd = GetNode("WallTileMap/DoorEnd") as Door;
		
		_player = GetNode("WallTileMap/Player") as Player;
	}

	private void _on_TutoKey_DialogEnded()
	{
		if (!_tutoKey)
		{
			_silverKey.Unlock();
			_silverKey.Visible = true;
			_deathTutoKey.Destroy();
			_deathTutoSwitch.Visible = true;
		}
	}

	private void _on_SwitchLeft_SwitchTurned(bool val)
	{
		if (!_tutoSwitch && _switchLeft.State && _switchRight.State)
		{
			_doorTutoSwitch.Open = true;
			_tutoSwitch = true;
		}
	}

	private void _on_SwitchRight_SwitchTurned(bool val)
	{
		if (!_tutoSwitch && _switchLeft.State && _switchRight.State)
		{
			_doorTutoSwitch.Open = true;
			_tutoSwitch = true;
		}
	}

	private void _on_TutoSwitch_DialogEnded()
	{
		_switchLeft.Locked = false;
		_switchLeft.Visible = true;
		_switchLeftSmoke.CloneInCurrentScene(false);
		
		_switchRight.Locked = false;
		_switchRight.Visible = true;
		_switchRightSmoke.CloneInCurrentScene(false);
		
		_deathTutoSwitch.Destroy();
		_deathTutoEnemy.Visible = true;
	}

	private void _on_TutoEnemy_DialogEnded()
	{
		_doorTutoEnemy.Open = true;
		_deathTutoEnemy.Destroy();
		_deathTutoEnd.Visible = true;
		
		var attackScancode = Convert.ToUInt32(_Keybinds.Bindings["attack"]);
		var attackKey = "";
		switch (attackScancode)
		{
			case 1:
				attackKey = "Left Click";
				break;
			case 2:
				attackKey = "Right Click";
				break;
			default:
				attackKey = OS.GetScancodeString(attackScancode);
				break;
		}
		_hintUI.Text = "Press " + attackKey + " to attack."; 
	}
	
	private void _on_Skeleton_EnemyDeath()
	{
		_skeletonCounter -= 1;
		if (_skeletonCounter == 0)
		{
			_doorTutoEnd.Open = true;
			_hintUI.Text = "";
		}
	}
	
	private void _on_TutoEnd_DialogEnded()
	{
		_deathTutoEnd.Destroy();
		_player.EraseFromGame();
		
		var saveGame = new File();
		saveGame.Open("user://savegame.save", File.ModeFlags.Write);
		saveGame.Close();
		
		_LevelManager.SpawnInLobby();
	}
}

using Godot;
using System;

// Singleton: Keeps track of the score of the player
public class Score : Node
{
	public String KilledBy = "";
	public float Time = 0.0f;
	public int HitsTaken = 0;
	public int WeaponsUsed = 0;
	
	public bool TimerRunning;
	
	public override void _Process(float delta)
	{
		if (TimerRunning)
		{
			Time += delta;
		}
	}
	
	public void StartTimeCounter()
	{
		Time = 0.0f;
		TimerRunning = true;
	}
	
	public void StopTimeCounter()
	{
		TimerRunning = false;
	}
	
	public void ResetScore()
	{
		KilledBy = "";
		Time = 0.0f;
		HitsTaken = 0;
		WeaponsUsed = 0;
	}
}

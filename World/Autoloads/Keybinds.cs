using Godot;
using System;

public class Keybinds : Node
{
	private const String CONFIG_PATH_DEFAULT = "res://Config/keybinds.ini";
	private const String CONFIG_PATH = "user://keybinds.ini";
	private ConfigFile _configFile = null;
	
	public Godot.Collections.Dictionary Bindings = new Godot.Collections.Dictionary();
	
	public override void _Ready()
	{
		_configFile = new ConfigFile();
		
//		User config file exist
		if (_configFile.Load(CONFIG_PATH) == Error.Ok)
		{
			foreach (String key in _configFile.GetSectionKeys("keybinds"))
			{
				var keyValue = Convert.ToUInt32(_configFile.GetValue("keybinds", key));
				Bindings[key] = keyValue;
			}
		}
//		User config file doesn't exist: use the default one
		else if ((_configFile.Load(CONFIG_PATH) == Error.FileNotFound && _configFile.Load(CONFIG_PATH_DEFAULT) == Error.Ok))
		{
			foreach (String key in _configFile.GetSectionKeys("keybinds"))
			{
				var keyValue = Convert.ToUInt32(_configFile.GetValue("keybinds", key));
				Bindings[key] = keyValue;
			}
		}
		else
		{
			GD.PushError("CONFIG FILE NOT FOUND");
			GetTree().Quit();
		}
		
		SetGameBinds();
	}
	
	public void SetGameBinds()
	{
		foreach (String key in Bindings.Keys)
		{
			var val = (int)Bindings[key];
			
//			Erase the last setting registered
			var actionList = InputMap.GetActionList(key);
			if (!(actionList.Count == 0))
			{
				InputMap.ActionEraseEvent(key, actionList[0] as InputEvent);
			}

//			Check if key bind or mouse button bind
			if (val==1 || val==2)
			{
				var newKey = new InputEventMouseButton();
				newKey.ButtonIndex = val;
				InputMap.ActionAddEvent(key, newKey);
			}
			else
			{
				var newKey = new InputEventKey();
				newKey.Scancode = Convert.ToUInt32(val);
				InputMap.ActionAddEvent(key, newKey);
			}
		}
	}
	
	public void WriteGameBinds()
	{
		foreach (String key in Bindings.Keys)
		{
			var keyValue = Bindings[key];
			_configFile.SetValue("keybinds", key, keyValue);
		}
		_configFile.Save(CONFIG_PATH);
	}
}

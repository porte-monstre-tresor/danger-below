using Godot;
using System;
using Godot.Collections;

public class LevelManager : Node
{
	[Signal]
	delegate void DisplayVictory();
	
	[Signal]
	delegate void DisplayLevelCleared();
	
	private String MAIN_MENU_PATH = "res://World/UI/Menus/MainMenu/MainMenu.tscn";
	private String OPTIONS_MENU_PATH = "res://World/UI/Menus/OptionsMenu/OptionsMenu.tscn";
	private String TUTO_PATH = "res://World/Levels/Tutorial/Tutorial.tscn";
	private String LOBBY_PATH = "res://World/Levels/Lobby/Lobby.tscn";
	private String LOADING_SCREEN_PATH = "res://World/Levels/LoadingScreen/LoadingScreen.tscn";
	
	private String _levelBase = "res://World/Levels/Level/LevelBase.tscn";
	private String _hiddenShop = "res://World/Levels/Level/ShopLevel.tscn";
	
	private Score _Score = null;
	private Stats _Stats = null;
	private Weapons _Weapons = null;
	
	public int CurrentLevel;
	
	private Node _currentScene;
	private ResourceInteractiveLoader _loader;
	private int _timeMax = 1000; // ms
	
	private Control _loadingScreen = null;
	
	public override void _Ready()
	{
		_Score = GetNode("/root/Score") as Score;
		_Stats = GetNode("/root/PlayerStats") as Stats;
		_Weapons = GetNode("/root/Weapons") as Weapons;
		
		_loadingScreen = GetNode("LoadingScreen") as Control;
		
		var root = GetNode("/root");
		_currentScene = root.GetChild(root.GetChildCount() - 1);
	}
	
		public override void _Process(float delta)
	{
		var t = OS.GetTicksMsec();
		while (_loader != null && OS.GetTicksMsec() < (t + _timeMax))
		{
			var err = _loader.Poll();
			
			switch(err)
			{
				case Error.FileEof:
					var resource = _loader.GetResource() as PackedScene;
					_loader.Dispose();
					_loader = null;
					
					
					var root = GetNode("/root");
					_currentScene = resource.Instance();
					root.AddChild(_currentScene);
					_loadingScreen.Visible = false;
					break;
				
				case Error.Ok:
					_loadingScreen.Visible = true;
					break;
					
				default:
					GD.PushError("ERROR IN LOADING PROCESS");
					GetTree().Quit();
					_loader = null;
					break;
					
			}
		}
	}
	
	private void _gotoScene(String path)
	{
		_loader = ResourceLoader.LoadInteractive(path);
		if (_loader == null)
		{
			GD.PushError("ERROR IN LOADER GENERATION");
			GetTree().Quit();
		}
		
		SetProcess(true);
		_currentScene.QueueFree();

		_loadingScreen.Visible = true;
	}
	
	public void StartRun()
	{
		_Score.ResetScore();
		
		_Stats.ResetCurrencies();
		_Stats.Health = _Stats.MaxHealth;
		
		_Weapons.GenerateWeaponsDeck();
		CurrentLevel = 1;
		LevelBase.Size = 3;
		LevelBase.Diversity = 1;
		_gotoScene(_levelBase);
	}
	
	public void StartTutorial()
	{
		_Score.ResetScore();
	
		_Stats.ResetCurrencies();
		_Stats.Health = _Stats.MaxHealth;
		
		_Weapons.ResetWeaponsDeck();
		_gotoScene(TUTO_PATH);
	}
	
	public void SpawnInLobby()
	{
		_gotoScene(LOBBY_PATH);
	}
	
	public void GoToMainMenu()
	{
		_gotoScene(MAIN_MENU_PATH);
	}
	
	public void GoToOptionsMenu()
	{
		_gotoScene(OPTIONS_MENU_PATH);
	}
	
	public void LevelCleared()
	{
		EmitSignal("DisplayLevelCleared");
	}
	
	public void NextLevel()
	{
		CurrentLevel += 1;
		
		switch (CurrentLevel)
		{
			case 2:
				LevelBase.Diversity = 2;
				break;
			case 4:
				LevelBase.Diversity = 3;
				break;
			case 5:
				LevelBase.Size = 5;
				break;
			default:
				break;
		}
		
		if (CurrentLevel % 3 == 0)
		{
			_gotoScene(_hiddenShop);
		}
		else
		{
			_gotoScene(_levelBase);
		}
	}
	
	public void Victory()
	{
		EmitSignal("DisplayVictory");
	}
}

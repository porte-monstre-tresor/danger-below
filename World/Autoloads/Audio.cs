using Godot;
using System;

public class Audio : AudioStreamPlayer
{
	private const String CONFIG_PATH_DEFAULT = "res://Config/audio.ini";
	private const String CONFIG_PATH = "user://audio.ini";
	private ConfigFile _configFile = null;
	
	public Godot.Collections.Dictionary Settings = new Godot.Collections.Dictionary();
	
	public override void _Ready()
	{
		_configFile = new ConfigFile();
//
		if (_configFile.Load(CONFIG_PATH) == Error.Ok)
		{
			foreach (String key in _configFile.GetSectionKeys("audio"))
			{
				var val = Convert.ToInt32(_configFile.GetValue("audio", key));
				Settings[key] = val;
				if (val>-30)
				{
					AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex(key), val);
					AudioServer.SetBusMute(AudioServer.GetBusIndex(key), false);
				}
				else
				{
					AudioServer.SetBusMute(AudioServer.GetBusIndex(key), true);
				}
			}
		}
		else if ((_configFile.Load(CONFIG_PATH) == Error.FileNotFound && _configFile.Load(CONFIG_PATH_DEFAULT) == Error.Ok))
		{
			foreach (String key in _configFile.GetSectionKeys("audio"))
			{
				var val = Convert.ToInt32(_configFile.GetValue("audio", key));
				Settings[key] = val;
				if (val>-30)
				{
					AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex(key), val);
					AudioServer.SetBusMute(AudioServer.GetBusIndex(key), false);
				}
				else
				{
					AudioServer.SetBusMute(AudioServer.GetBusIndex(key), true);
				}
			}
		}
		else
		{
			GD.PushError("CONFIG FILE NOT FOUND");
			GetTree().Quit();
		}
		
		Playing = true;
	}
	
	public void SetVolume(String name, int val)
	{
		Settings[name] = val-30;
		if (val>0)
		{
			AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex(name), val-30);
			AudioServer.SetBusMute(AudioServer.GetBusIndex(name), false);
		}
		else
		{
			AudioServer.SetBusMute(AudioServer.GetBusIndex(name), true);
		}
	}
	
	public void WriteAudioSettings()
	{
		foreach(String key in Settings.Keys)
		{
			_configFile.SetValue("audio", key, (int)Settings[key]);
		}
		_configFile.Save(CONFIG_PATH);
	}

	private void _on_AudioStreamPlayer_finished()
	{
		Playing = true;
	}
}


using Godot;
using System;

public class Looter : Node
{
	private const String SILVER_KEY_PATH = "res://Entities/Loots/SilverKey/SilverKey.tscn";
	private const String GOLDEN_KEY_PATH = "res://Entities/Loots/GoldenKey/GoldenKey.tscn";
	private const String COIN_PATH = "res://Entities/Loots/Coin/Coin.tscn";
	private const String HEART_PATH = "res://Entities/Loots/Heart/Heart.tscn";
	private const String DROPPED_WEAPON_PATH = "res://Entities/Loots/DroppedWeapon/DroppedWeapon.tscn";

	private PackedScene SilverKeyScene;
	private PackedScene GoldenKeyScene;
	private PackedScene CoinScene;
	private PackedScene HeartScene;
	private PackedScene DroppedWeaponScene;
	
	private Godot.Collections.Dictionary _loots = new Godot.Collections.Dictionary();

	public override void _Ready()
	{
		SilverKeyScene = GD.Load(SILVER_KEY_PATH) as PackedScene;
		GoldenKeyScene = GD.Load(GOLDEN_KEY_PATH) as PackedScene;
		CoinScene = GD.Load(COIN_PATH) as PackedScene;
		HeartScene = GD.Load(HEART_PATH) as PackedScene;
		DroppedWeaponScene = GD.Load(DROPPED_WEAPON_PATH) as PackedScene;
		
		_loots.Add("SilverKey", SilverKeyScene);
		_loots.Add("GoldenKey", GoldenKeyScene);
		_loots.Add("Coin", CoinScene);
		_loots.Add("Heart", HeartScene);
		_loots.Add("Axe", DroppedWeaponScene);
		_loots.Add("Regular Sword", DroppedWeaponScene);
		_loots.Add("Spear", DroppedWeaponScene);
		_loots.Add("Knife", DroppedWeaponScene);
		_loots.Add("Nailed Bat", DroppedWeaponScene);
	}

	public void DropItemAt(Godot.Collections.Array<String> drops, NodePath node, Vector2 pos)
	{
		Node2D target = GetNode("/root/World/WallTileMap") as Node2D;
		foreach (String name in drops)
		{
			var item = _loots[name] as PackedScene;
			var drop = item.Instance() as Node2D;
			if (drop is DroppedWeapon weapon)
			{
				weapon.WeaponName = name;
			}
			var offset = new Vector2((GD.Randf()-0.5f)*10.0f, (GD.Randf()-0.5f)*10.0f);
			drop.GlobalPosition = pos + offset;
			target.CallDeferred("add_child", drop);
		}
	}
}

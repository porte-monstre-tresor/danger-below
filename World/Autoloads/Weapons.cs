using Godot;
using System;
using System.Collections.Generic;
using System.Collections;

// Singleton: Collect all of the weapons and their meta-data
public class Weapons : Node
{
//	Tells the player to equip a specific weapon
	[Signal]
	delegate void EquipWeapon(String name);
	
	[Signal]
	delegate void UnequipWeapon();
	
//	Change the weapon displayed on the top card of the deck
	[Signal]
	delegate void SetDeck(int amount, String weapon);

//	Add an amount of cards to the deck 
	[Signal]
	delegate void EditDeck(int offset, String weapon);
	
	private const String CONFIG_PATH_DEFAULT = "res://Config/weapons.ini";
	private const String CONFIG_PATH = "user://weapons.ini";
	private ConfigFile _configFile = null;

//	Path of the weapons scenes
	private const String KNIFE_PATH = "res://Entities/Weapons/Knife/Knife.tscn";
	private const String AXE_PATH = "res://Entities/Weapons/Axe/Axe.tscn";
	private const String REGULAR_SWORD_PATH = "res://Entities/Weapons/RegularSword/RegularSword.tscn";
	private const String RUSTY_SWORD_PATH = "res://Entities/Weapons/RustySword/RustySword.tscn";
	
//	Name of existing weapons
	public const String Knife = "Knife";
	public const String Axe = "Axe";
	public const String RegularSword = "Regular Sword"; 
	public const String Spear = "Spear";
	public const String NailedBat = "Nailed Bat";
	public const String RustySword = "Rusty Sword";
	
	public Godot.Collections.Array<String> WeaponsList = new Godot.Collections.Array<String>{
		Knife, Axe, RegularSword, Spear, NailedBat
	};
	
//	Shuffled list of weapons names
	private List<String> _deck = new List<String>();
	
	public Dictionary<String, int> Hand = new Dictionary<String, int>();

	private int _indexInDeck = 0;
	
//	Check if the weapon is usable
	public bool IsEquiped = false;
	public bool UsingDefault = false;
	
	public override void _Ready()
	{
		_configFile = new ConfigFile();
		
		if (_configFile.Load(CONFIG_PATH) == Error.Ok)
		{
			foreach (String weapon in _configFile.GetSectionKeys("weapons"))
			{
				var weaponName = weapon.Replace("_", " ");
				var amount = Convert.ToInt32(_configFile.GetValue("weapons", weapon));
				Hand[weaponName] = amount;
			}
		}
		else if ((_configFile.Load(CONFIG_PATH) == Error.FileNotFound && _configFile.Load(CONFIG_PATH_DEFAULT) == Error.Ok))
		{
			foreach (String weapon in _configFile.GetSectionKeys("weapons"))
			{
				var weaponName = weapon.Replace("_", " ");
				var amount = Convert.ToInt32(_configFile.GetValue("weapons", weapon));
				Hand[weaponName] = amount;
			}
		}
		else
		{
			GD.PushError("CONFIG FILE NOT FOUND");
			GetTree().Quit();
		}
	}
	
	public void WriteWeaponsRepartition()
	{
		foreach (String weapon in Hand.Keys)
		{
			var weaponName = weapon.Replace(" ", "_");
			var amount = Hand[weapon];
			_configFile.SetValue("weapons", weaponName, amount);
		}
		_configFile.Save(CONFIG_PATH);
	}
	
	public void ResetWeaponsDeck()
	{
		_deck = new List<String>();
	}
	
//	Create the deck of shuffled weapons names
	public void GenerateWeaponsDeck()
	{
//		Reset the deck
		_indexInDeck = 0;
		
//		Create a list of weapons
		List<String> deck = new List<String>();
//		}
		foreach (String weapon in Hand.Keys)
		{
			for (int i=0; i<(int)(Hand[weapon]); i++)
			{
				deck.Add(weapon);
			}
		}
		
//		Shuffle it randomly
		_deck = _shuffle(deck);
	}
	
//	Shuffle a list of string
	private List<String> _shuffle<String>(List<String> deck)  
	{  
		var rng = new RandomNumberGenerator();
		rng.Randomize();
		int n = deck.Count;  
		while (n > 1) {  
			n--;  
			int k = rng.RandiRange(0, n);  
			String val = deck[k];  
			deck[k] = deck[n];  
			deck[n] = val;  
		}

		return deck;
	}
	
	public void GetFirstWeapon()
	{
//		Tells the player to equip the n-th weapon
		if (_indexInDeck < _deck.Count)
		{
			EmitSignal("EquipWeapon", _deck[_indexInDeck]);
			UsingDefault = false;
		}
		else
		{
			EmitSignal("EquipWeapon", RustySword);
			UsingDefault = true;
		}
		
		IsEquiped = true;
		
//		If there is a next card in deck, tells the UI to display it on top of the deck
		if (_indexInDeck<_deck.Count-1)
		{
			EmitSignal("SetDeck", _deck.Count-_indexInDeck-1, _deck[_indexInDeck+1]);
		}
		else
		{
			EmitSignal("SetDeck", 0, "");
		}
	}
	
	public async void GetNextWeapon()
	{
//		Player can't attack anymore
		IsEquiped = false;

//		Cooldown
		await ToSignal(GetTree().CreateTimer(0.5f, false), "timeout");
		
//		Can attack again
		IsEquiped = true;
		
		if (_indexInDeck < _deck.Count - 1)
		{
			_indexInDeck += 1;
			UsingDefault = false;
			
//			Inform the player of the next weapon
			EmitSignal("EquipWeapon", _deck[_indexInDeck]);
			
//			Inform the UI of the next weapon
			EmitSignal("EditDeck", -1, (_indexInDeck < _deck.Count - 1) ? _deck[_indexInDeck+1] : "");
		}
		else
		{
			UsingDefault = true;
			
//			Inform the player of the default weapon
			EmitSignal("EquipWeapon", RustySword);
			
//			No need to inform the UI, the default weapon is displayed in background
		}
	}
	
	public void InsertNext(String weaponName)
	{
		if (_indexInDeck < _deck.Count - 1)
		{
			_deck.Insert(_indexInDeck+1, weaponName);
		}
		else
		{
			_deck.Add(weaponName);
		}
		EmitSignal("EditDeck", 1, _deck[_indexInDeck+1]);
	}
	
//	Get the scene corresponding of the name of a weapon
	public PackedScene GetWeaponScene(String name)
	{
		String path = "";
		switch (name)
		{
			case Knife:
				path = KNIFE_PATH;
				break;
			case Axe:
				path = AXE_PATH;
				break;
			case RegularSword:
				path = REGULAR_SWORD_PATH;
				break;
			case RustySword:
				path = RUSTY_SWORD_PATH;
				break;
			default:
				break;
		}
		return GD.Load(path) as PackedScene;
	}
}

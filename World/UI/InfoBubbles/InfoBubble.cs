using Godot;
using System;

public class InfoBubble : Node2D
{
	[Export]
	public Texture BubbleTexture = null;
	
	[Export]
	public int Hframes = 1;
	
	[Export]
	public int Vframes = 1;
	
	[Export]
	public int Frame = 0;
	
	private Control _bubble1 = null;
	private Control _bubble2 = null;
	private Control _bubble3 = null;
	private Sprite _sprite = null;
	
	private AnimationPlayer _spawnAnimation = null;

	public override void _Ready()
	{
		_sprite = GetNode("Sprite") as Sprite;
		_bubble1 = GetNode("Bubble1") as Control;
		_bubble2 = GetNode("Bubble2") as Control;
		_bubble3 = GetNode("Bubble3") as Control;
		
		_sprite.Visible = false;
		_bubble1.Visible = false;
		_bubble2.Visible = false;
		_bubble3.Visible = false;
		
		_spawnAnimation = GetNode("SpawnAnimation") as AnimationPlayer;
	
		_sprite.Texture = BubbleTexture;
		_sprite.Hframes = Hframes;
		_sprite.Vframes = Vframes;
		_sprite.Frame = Frame;
	}
	
	public void Display()
	{
		_spawnAnimation.Stop();
		_spawnAnimation.Play("Display");
	}
	
	public void Hide()
	{
		_spawnAnimation.Stop();
		_spawnAnimation.Play("Hide");
	}
}

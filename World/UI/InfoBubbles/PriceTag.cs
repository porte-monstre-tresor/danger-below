using Godot;
using System;

public class PriceTag : Node2D
{
	[Export]
	public Texture CurrencyTexture = null;
	
	[Export]
	private int _costValue = 1;
	public int CostValue
	{
		get { return _costValue; }
		set 
		{
			_costValue = value;
			_costLabel.Text = "x " + value.ToString();
		}
	}
	
	private TextureRect _currencyImage = null;
	private Label _costLabel = null;

	public override void _Ready()
	{
		_currencyImage = GetNode("WoodenFrame/PaperBackground/HBoxContainer/CurrencyContainer/CurrencyTexture") as TextureRect;
		_costLabel = GetNode("WoodenFrame/PaperBackground/HBoxContainer/PaperLabel") as Label;
	
		_currencyImage.Texture = CurrencyTexture;
		_costLabel.Text = "x " + CostValue.ToString();
	}
}

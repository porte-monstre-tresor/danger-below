using Godot;
using System;

public class SpinBox : Control
{
	[Signal]
	delegate void ValueChanged(float Value);
	
	[Export]
	public int Value = 10;
	
	[Export]
	public int UpperLimit = 100;
	
	[Export]
	public int LowerLimit = 0;

	private Button _upButton = null;
	private Button _downButton = null;
	private LineEdit _lineEdit = null;
	private String _lastText;
	
	public override void _Ready()
	{
		_upButton = GetNode("HBoxContainer/Spin/UpContainer/Up") as Button;
		_downButton = GetNode("HBoxContainer/Spin/DownContainer/Down") as Button;
		_lineEdit = GetNode("HBoxContainer/Box/LineEdit") as LineEdit;
		
		_lineEdit.Text = Value.ToString();
		_lastText = Value.ToString();
	}
	
	public void SetValue(int val)
	{
		Value = val;
		_lineEdit.Text = val.ToString();
		_lastText = val.ToString();
	}

	private void _on_Up_pressed()
	{
		if (Value<UpperLimit)
		{
			Value += 1;
			_lineEdit.Text = Value.ToString();
			_lastText = Value.ToString();
			EmitSignal("ValueChanged", Value);
		}
		
		_downButton.Disabled = false;
		_upButton.Disabled = (Value==UpperLimit);
	}
	
	private void _on_Down_pressed()
	{
		if (Value>LowerLimit)
		{
			Value -= 1;
			_lineEdit.Text = Value.ToString();
			_lastText = Value.ToString();
			EmitSignal("ValueChanged", Value);
		}
		
		_upButton.Disabled = false;
		_downButton.Disabled = (Value==LowerLimit);
	}

	private void _on_LineEdit_text_entered(String new_text)
	{
		int val;
		bool isInt = int.TryParse(new_text, out val);
		
		if (isInt && val>=LowerLimit && val<=UpperLimit)
		{
			Value = val;
			_lastText = new_text;
			
			_upButton.Disabled = (Value==UpperLimit);
			_downButton.Disabled = (Value==LowerLimit);
			EmitSignal("ValueChanged", Value);
		}
		else
		{
			_lineEdit.Text = _lastText;
		}
	}
}







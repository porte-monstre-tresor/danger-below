using Godot;
using System;

public class BasicButton : Button
{
	private ColorRect _effect = null;
	private AnimationPlayer _animationPlayer = null;
	
	public override void _Ready()
	{
		_effect = GetNode("ColorRect") as ColorRect;
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
	}
	
	private void _on_BasicButton_button_down()
	{
		_effect.Visible = !_effect.Visible;
		_animationPlayer.Play("Press");
	}

	private async void _on_BasicButton_button_up()
	{
		if (!ToggleMode)
		{
//			await ToSignal(GetTree().CreateTimer(0.2f), "timeout");
			_effect.Visible = false;
			_animationPlayer.Play("Release");
		}
	}
	
	public void ForceUntoggle()
	{
		_effect.Visible = false;
		Pressed = false;
	}
}





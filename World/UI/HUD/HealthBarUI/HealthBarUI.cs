using Godot;
using System;

public class HealthBarUI : Control
{
	private Stats _stats = null;
	private TextureRect _heartUIFull = null;
	private TextureRect _heartUIHalf = null;
	private TextureRect _heartUIEmpty = null;
	private Control _frame = null;

	private int _MaxHearts = 4;
	private int MaxHearts
	{
		get { return _MaxHearts; }
		set { _setMaxHearts(value); }
	}

	private void _setMaxHearts(int maxHearts)
	{
		_MaxHearts = Math.Max(maxHearts, 1);
		this.Hearts = Math.Min(Hearts, maxHearts);
		if ( _heartUIEmpty != null )
		{
			var heartSize = _heartUIEmpty.RectSize;
			heartSize.x = (MaxHearts/2 + MaxHearts%2) * 16;
			_heartUIEmpty.RectSize = heartSize;
			
			var frameSize = _frame.RectSize;
			frameSize.x = (MaxHearts/2 + MaxHearts%2) * 16 + 11;
			_frame.RectSize = frameSize;
		}
	}

	public int _Hearts = 4;
	public int Hearts
	{
		get { return _Hearts; }
		set { _setHearts(value); }
	}

	private void _setHearts(int hearts)
	{
		_Hearts = Mathf.Clamp(hearts, 0, MaxHearts);
		if ( _heartUIFull != null && _heartUIHalf != null)
		{
			var size = _heartUIFull.RectSize;
			size.x = (_Hearts/2) * 16;
			_heartUIFull.RectSize = size;
			
			size = _heartUIHalf.RectSize;
			size.x = (_Hearts/2 + _Hearts%2) * 16;
			_heartUIHalf.RectSize = size;
		}
	}

	public override void _Ready()
	{	
		_stats = GetNode("/root/PlayerStats") as Stats;
		_heartUIFull = GetNode("HeartUIFull") as TextureRect;
		_heartUIHalf = GetNode("HeartUIHalf") as TextureRect;
		_heartUIEmpty = GetNode("HeartUIEmpty") as TextureRect;
		_frame = GetNode("Frame") as Control;

		this.MaxHearts = _stats.MaxHealth;
		this.Hearts = _stats.Health;
		_stats.Connect("HealthChanged", this, "_setHearts");
		_stats.Connect("MaxHealthChanged", this, "_setMaxHearts");
	}
}

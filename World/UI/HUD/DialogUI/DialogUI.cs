using Godot;
using System;
using Godot.Collections;

public class DialogUI : Control
{
	[Signal]
	delegate void DialogEnded();
	
	private Control _characterContainer = null;
	private RichTextLabel _nameLabel = null;
	private RichTextLabel _textLabel = null;
	private Tween _tween = null;
	private TextureRect _finishedIndicator = null;
	
	private String _lastCharacter = "";
	
	private String _currentFile = "";
	private Dictionary _currentDialog = null;
	private int _currentLine = 0;
	
	public override void _Ready()
	{	
		_characterContainer = GetNode("Frame/HBoxContainer/LeftBackground/CenterContainer") as Control;
		_nameLabel = GetNode("Frame/HBoxContainer/RightBackground/VBoxContainer/NameLabel") as RichTextLabel;
		_textLabel = GetNode("Frame/HBoxContainer/RightBackground/VBoxContainer/TextContainer/TextLabel") as RichTextLabel;
		_tween = GetNode("Tween") as Tween;
		_finishedIndicator = GetNode("Frame/FinishedIndicator") as TextureRect;
		
		_finishedIndicator.Visible = false;
	}
	
	public void Play(String file_name)
	{
		if (file_name != _currentFile)
		{
			_currentFile = file_name;
			
			var file = new File();
			file.Open(file_name, File.ModeFlags.Read);
			
			String text = file.GetAsText();
			var jsonFile = JSON.Parse(text).Result;
			_currentDialog = jsonFile as Godot.Collections.Dictionary;
			file.Close();
			
			_currentLine = 0;
			
			var line = _currentDialog[_currentLine.ToString()] as Godot.Collections.Dictionary;
			
			Visible = true;
			_displayText(line["character"] as String, line["name"] as String, line["text"] as String);
		}
		
		else
		{
			_currentLine += 1;
			if (_currentLine >= _currentDialog.Keys.Count)
			{
				EmitSignal("DialogEnded");
				Stop();
			}
			else
			{
				var line = _currentDialog[_currentLine.ToString()] as Godot.Collections.Dictionary;
				_displayText(line["character"] as String, line["name"] as String, line["text"] as String);
			}
		}
	}
	
	public void Stop()
	{
		_currentFile = "";
		Visible = false;
	}

	private void _displayText(String character, String name, String text)
	{
		if (character != _lastCharacter)
		{
			foreach (Sprite sprite in _characterContainer.GetChildren())
			{
				sprite.Visible = false;
			}
			
			_lastCharacter = character;
			var newChar = _characterContainer.GetNode(character) as Sprite;
			newChar.Visible = true;
		}
		
		_finishedIndicator.Visible = false;
		
		_nameLabel.Text = name;
		
		_textLabel.PercentVisible = 0;
		_textLabel.Text = text;
		_tween.PlaybackSpeed = (float) (35f / (float)text.Length);
		_tween.InterpolateProperty(_textLabel, "percent_visible", 0, 1, 1, 
			(Tween.TransitionType)0, 
			(Tween.EaseType)2
		);
		_tween.Start();
	}
	
	private void _on_Tween_tween_completed(Godot.Object @object, NodePath key)
	{
		_finishedIndicator.Visible = true;
	}
}



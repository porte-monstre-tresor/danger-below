using Godot;
using System;

public class WeaponsDeckUI : Control
{
	private const String CARD_PATH = "res://World/UI/HUD/WeaponsDeckUI/Card.tscn";
	private PackedScene _cardScene = null;
	
	private Control _hbox = null;
	private Control _cardsBox = null;
	private Label _weaponsLeftLabel = null;
	private Weapons _Weapons;
	
	private int _maxCardsDisplayed = 20;
	
	private int _cards;
	public int Cards
	{
		get { return _cards; }
		set
		{
			_weaponsLeftLabel.Text = "x "+value.ToString();
			_cards = value;
			
			foreach (Control child in _cardsBox.GetChildren())
			{
				child.Visible = false;
				child.QueueFree();
			}
			
			var max = Mathf.Min(value, _maxCardsDisplayed);
			if (max>0)
			{
				var position = _hbox.RectPosition;
				position.y = -56 - (max-1)*3;
				_hbox.RectPosition = position;
				
				for (int i=0; i<max-1; i++)
				{
					var card = _cardScene.Instance() as Card;
					_cardsBox.AddChild(card);
				}
				var topCard = _cardScene.Instance() as Card;
				_cardsBox.AddChild(topCard);
				topCard.DisplayWeapon(_weaponDisplayed);
			}
		}
	}
	
	private String _weaponDisplayed;
	public String WeaponDisplayed
	{
		get { return _weaponDisplayed; }
		set
		{
			var index = Mathf.Min(_cards, _maxCardsDisplayed) - 1;
			if (index>=0)
			{
				var topCard = _cardsBox.GetChildren()[index] as Card;
				topCard.DisplayWeapon(value);
			}
			_weaponDisplayed = value;
		}
	}
	
	public override void _Ready()
	{
//		Singleton (instanciated, yes it's a workaround)
		_Weapons = GetNode("/root/Weapons") as Weapons;
		
//		Container of the cards
		_hbox = GetNode("DeckContainer") as Control;
		_cardsBox = GetNode("DeckContainer/CardsContainer") as Control;
		_weaponsLeftLabel = GetNode("DeckContainer/Label") as Label;
		
//		Scene of a card
		_cardScene = GD.Load(CARD_PATH) as PackedScene;
		
//		Signals from the weapons singleton to the UI
		_Weapons.Connect("SetDeck", this, "_setDeck");
		_Weapons.Connect("EditDeck", this, "_editDeck");
	}

	private void _setDeck(int amount, String weapon)
	{
		WeaponDisplayed = weapon;
		Cards = amount;
	}
	
	private void _editDeck(int offset, String weapon)
	{
		if (weapon!="")
		{
			WeaponDisplayed = weapon;
		}
		Cards += offset;
	}
}

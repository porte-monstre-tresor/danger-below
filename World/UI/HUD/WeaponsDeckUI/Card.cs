using Godot;
using System;

public class Card : Control
{
	private Control _weaponsSprites = null;
	private Weapons _Weapons = null;
	
	public override void _Ready()
	{
//		Weapons singleton
		_Weapons = GetNode("/root/Weapons") as Weapons;
		
//		Node of all the possible weapons
		_weaponsSprites = GetNode("TextureRect/WeaponsSprites") as Control;
		
//		Hide everything to start
		foreach (TextureRect sprite in _weaponsSprites.GetChildren())
		{
			sprite.Visible = false;
		}
	}

	public void DisplayWeapon(String weaponName)
	{
//		Hide all the possible weapons
		foreach (TextureRect sprite in _weaponsSprites.GetChildren())
		{
			sprite.Visible = false;
		}
		
//		Show the desired weapon
		var weaponSprite = _weaponsSprites.GetNode(weaponName) as TextureRect;
		weaponSprite.Visible = true;
	}
}

using Godot;
using System;

public class HUD : CanvasLayer
{
	private LevelManager _LevelManager = null;
	private Stats _PlayerStats = null;
	
	private GameOverOverlay _gameOverOverlay = null;
	private VictoryOverlay _victoryOverlay = null;
	private LevelClearedOverlay _levelClearedOverlay = null;
	private PauseOverlay _pauseOverlay = null;
	private HealthBarUI _healthBarUI = null;
	private WalletUI _walletUI = null;
	private WeaponsDeckUI _weaponsDeckUI = null;
	private Fade _fade = null;
	
	enum States
	{
		Running,
		Paused,
		EndGame
	}
	private States _state;
	
	[Export(PropertyHint.Enum, "Game, Lobby")]
	private int _context = 0;
	
	public override void _Ready()
	{
		_LevelManager = GetNode("/root/LevelManager") as LevelManager;
		_PlayerStats = GetNode("/root/PlayerStats") as Stats;
		
		_gameOverOverlay = GetNode("GameOverOverlay") as GameOverOverlay;
		_victoryOverlay = GetNode("VictoryOverlay") as VictoryOverlay;
		_levelClearedOverlay = GetNode("LevelClearedOverlay") as LevelClearedOverlay;
		_pauseOverlay = GetNode("PauseOverlay") as PauseOverlay;
		_healthBarUI = GetNode("HealthBarUI") as HealthBarUI;
		_walletUI = GetNode("WalletUI") as WalletUI;
		_weaponsDeckUI = GetNode("WeaponsDeckUI") as WeaponsDeckUI;
		_fade = GetNode("Fade") as Fade;
		
		_LevelManager.Connect("DisplayVictory", this, "_displayVictory");
		_LevelManager.Connect("DisplayLevelCleared", this, "_displayLevelCleared");
		_PlayerStats.Connect("DisplayGameOver", this, "_displayGameOver");
		_pauseOverlay.Connect("Resume", this, "_resume");
		
		_healthBarUI.Visible = (_context == 0);
		_walletUI.Visible = (_context == 0);
		_weaponsDeckUI.Visible = (_context == 0);
		
		_state = States.Running;
		
		_fade.Play();
	}
	
	public override void _Input(InputEvent inputEvent)
	{
		if (inputEvent.IsActionPressed("pause"))
		{
			if (_state == States.Running)
			{
				_state = States.Paused;
				_pauseOverlay.Display();
			}
			else if (_state == States.Paused)
			{
				_state = States.Running;
				_pauseOverlay.Cancel();
			}
		}
	}
	
	private void _displayVictory()
	{
		_state = States.EndGame;
		_victoryOverlay.Display();
	}
	
	private void _displayLevelCleared()
	{
		_state = States.EndGame;
		_levelClearedOverlay.Display();
	}
	
	private void _displayGameOver()
	{
		_state = States.EndGame;
		_gameOverOverlay.Display();
	}
	
	private void _resume()
	{
		_state = States.Running;
		_pauseOverlay.Cancel();
	}
}

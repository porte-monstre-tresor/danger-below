using Godot;
using System;

public class HintUI : Control
{
	private Label _label = null;
	
	private String _text = "";
	public String Text
	{
		get { return _text; }
		set {
			_text = value;
			if (value == "")
			{
				Visible = false;
			}
			else
			{
				Visible = true;
				_label.Text = value;
			}
		}
	}
	
	public override void _Ready()
	{
		_label = GetNode("HintLabel") as Label;
		Visible = false;
	}
}

using Godot;
using System;

public class WalletUI : Control
{
	private Stats _Stats = null;
	
	private Control _frame = null;
	private Label _silverKeyLabel = null;
	private Label _goldenKeyLabel = null;
	private Label _coinLabel = null;
	
	public override void _Ready()
	{
		_Stats = GetNode("/root/PlayerStats") as Stats;
		
		_frame = GetNode("Frame") as Control;
		_silverKeyLabel = GetNode("ItemsContainer/SilverKeyContainer/Label") as Label;
		_goldenKeyLabel = GetNode("ItemsContainer/GoldenKeyContainer/Label") as Label;
		_coinLabel = GetNode("ItemsContainer/CoinContainer/Label") as Label;
		
		_Stats.Connect("SetWalletUI", this, "_setCurrency");
		_Stats.UpdateWalletUI();
	}
	
	private void _setCurrency(String item, int amount)
	{
		switch (item)
		{
			case "SilverKey":
				_silverKeyLabel.Text = "x " + amount.ToString();
				break;
			case "GoldenKey":
				_goldenKeyLabel.Text = "x " + amount.ToString();
				break;
			case "Coin":
				_coinLabel.Text = "x " + amount.ToString();
				break;
			default:
				break;
		}
		
	}
}

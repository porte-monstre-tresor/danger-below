using Godot;
using System;

public class WeaponsShop : ShopUI
{
	private const String AMOUNT_OF_WEAPON_PATH = "res://World/UI/Menus/ShopsMenus/WeaponsShop/AmountOfWeapon.tscn";
	private const String TOP_FRAME_PATH = "res://World/UI/Frames/SplitFrame/TopFrame.tscn";
	private const String H_SPLIT_PATH = "res://World/UI/Frames/SplitFrame/HSplit.tscn";
	private const String DOWN_FRAME_PATH = "res://World/UI/Frames/SplitFrame/DownFrame.tscn";
	
	private Weapons _Weapons = null;
	private Control _weaponsOptions = null;
	
	public override void _Ready()
	{
		Visible = false;
		
		_Weapons = GetNode("/root/Weapons") as Weapons;
		_weaponsOptions = GetNode("VBoxContainer/Weapons/Details") as Control;
		
		var amountOfWeaponScene = GD.Load(AMOUNT_OF_WEAPON_PATH) as PackedScene;
		var topFrameScene = GD.Load(TOP_FRAME_PATH) as PackedScene;
		var hSplitScene = GD.Load(H_SPLIT_PATH) as PackedScene;
		var downFrameScene = GD.Load(DOWN_FRAME_PATH) as PackedScene;
		
		var i = 0;
		
		foreach(String weaponName in _Weapons.Hand.Keys)
		{
			var amountOfWeapon = amountOfWeaponScene.Instance() as AmountOfWeapon;
			Control frame;
			
			if (i==0)
			{
				amountOfWeapon.MarginTop = 4;
				amountOfWeapon.MarginBottom = 4;
				frame = topFrameScene.Instance() as Control;
			}
			else if (i==_Weapons.Hand.Count-1)
			{
				amountOfWeapon.MarginTop = -3;
				amountOfWeapon.MarginBottom = -3;
				frame = downFrameScene.Instance() as Control;
			}
			else
			{
				frame = hSplitScene.Instance() as Control;
			}
			
			_weaponsOptions.AddChild(frame);
			frame.AddChild(amountOfWeapon);
			amountOfWeapon.SetWeaponAndId(weaponName);
			i+=1;
		}
	}
	
	private void _on_SaveButton_pressed()
	{
		_Weapons.WriteWeaponsRepartition();
	}

	private void _on_CloseButton_pressed()
	{
		Visible = false;
	}
}


using Godot;
using System;

public class AmountOfWeapon : HBoxContainer
{
	private Weapons _Weapons = null;
	
	private Label _label = null;
	private SpinBox _spinBox = null;
	
	private String _weaponName;
	
	public override void _Ready()
	{
		_Weapons = GetNode("/root/Weapons") as Weapons;
		_label = GetNode("LabelContainer/Label") as Label;
		_spinBox = GetNode("SpinBoxContainer/SpinBox") as SpinBox;
	}

	public void SetWeaponAndId(String weaponName)
	{
		_label.Text = "Amount of " + weaponName;
		
		_weaponName = weaponName;
		_spinBox.SetValue((int)(_Weapons.Hand[weaponName]));
	}
	
	private void _on_SpinBox_ValueChanged(float Value)
	{
		_Weapons.Hand[_weaponName] = (int)Value;
	}
}





using Godot;
using System;

public class AudioTab : Tab
{
	private Audio _Audio = null;
	
	private SpinBox _generalSpinBox = null;
	private SpinBox _musicSpinBox = null;
	private SpinBox _sfxSpinBox = null;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_Audio = GetNode("/root/Audio") as Audio;
		
		_generalSpinBox = GetNode("Settings/TopFrame/GeneralContainer/SpinBoxContainer/SpinBox") as SpinBox;
		_musicSpinBox = GetNode("Settings/HSplit/MusicContainer/SpinBoxContainer/SpinBox") as SpinBox;
		_sfxSpinBox = GetNode("Settings/DownFrame/SFXContainer/SpinBoxContainer/SpinBox") as SpinBox;
		
//		Values used by Godot sound system between -30 and 0, we translate it in UI between 0 and 30 for clarity
		_generalSpinBox.SetValue((int)_Audio.Settings["Master"] + 30);
		_musicSpinBox.SetValue((int)_Audio.Settings["Music"] + 30);
		_sfxSpinBox.SetValue((int)_Audio.Settings["SFX"] + 30);
	}

	private void _on_GeneralSpinBox_ValueChanged(int val)
	{
		_Audio.SetVolume("Master", val);
	}

	private void _on_MusicSpinBox_ValueChanged(int val)
	{
		_Audio.SetVolume("Music", val);
	}

	private void _on_SFXSpinBox_ValueChanged(int val)
	{
		_Audio.SetVolume("SFX", val);
	}

	public override void Save()
	{
		_Audio.WriteAudioSettings();
	}
}


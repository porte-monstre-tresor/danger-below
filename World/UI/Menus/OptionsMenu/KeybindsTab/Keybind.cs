using Godot;
using System;

public class Keybind : HBoxContainer
{
	[Signal]
	delegate void ChangeBind(String command, uint bind);
	
	private Label _commandLabel = null;
	private Label _bindLabel = null;
	private BasicButton _button = null;
	
	private String _command;
	private uint _bindCode;
	private String _bindString;
	private bool _waitingInput = false;

	public override void _Ready()
	{
		_commandLabel = GetNode("CommandLabel") as Label;
		_bindLabel = GetNode("Key/Button/Label") as Label;
		_button = GetNode("Key/Button") as BasicButton;
	}
	
	public override void _Input(InputEvent inputEvent)
	{
		if (_waitingInput)
		{
			if (inputEvent is InputEventKey keyEvent)
			{
				if (keyEvent.Scancode != (uint)KeyList.Escape)
				{
					_bindCode = keyEvent.Scancode;
					_bindString = OS.GetScancodeString(keyEvent.Scancode);
					EmitSignal("ChangeBind", _command, _bindCode);
				}
			
				_bindLabel.Text = _bindString;
				
				_waitingInput = false;
				_button.ForceUntoggle();
			}
			else if (inputEvent is InputEventMouseButton mouseEvent)
			{
				switch(mouseEvent.ButtonIndex)
				{
					case 1:
						_bindCode = 1;
						_bindString = "Left Click";
						break;
					case 2:
						_bindCode = 2;
						_bindString = "Right Click";
						break;
					default:
						break;
				}
				
				EmitSignal("ChangeBind", _command, _bindCode);
				_bindLabel.Text = _bindString;
				
				_waitingInput = false;
				_button.ForceUntoggle();
			}
		}
	}

	public void SetKeyValue(String key, uint val)
	{
		_command = key;
		_bindCode = val;
		
		_commandLabel.Text = char.ToUpper(key[0]) + key.Substring(1);
		switch(val)
		{
			case 0:
				_bindString = "Unassigned";
				break;
			case 1:
				_bindString = "Left Click";
				break;
			case 2:
				_bindString = "Right Click";
				break;
			default:
				_bindString = OS.GetScancodeString(val);
				break;
		}
		
		_bindLabel.Text = _bindString;
		
	}

	private void _on_Button_toggled(bool button_pressed)
	{
		if (button_pressed)
		{
			_bindLabel.Text = "...";
			_waitingInput = true;
		}
	}
}


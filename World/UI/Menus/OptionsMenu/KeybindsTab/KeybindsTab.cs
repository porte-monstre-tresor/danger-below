using Godot;
using System;

public class KeybindsTab : Tab
{
	private const String KEYBIND_PATH = "res://World/UI/Menus/OptionsMenu/KeybindsTab/Keybind.tscn";
	private const String TOP_FRAME_PATH = "res://World/UI/Frames/SplitFrame/TopFrame.tscn";
	private const String H_SPLIT_PATH = "res://World/UI/Frames/SplitFrame/HSplit.tscn";
	private const String DOWN_FRAME_PATH = "res://World/UI/Frames/SplitFrame/DownFrame.tscn";
	
	private Keybinds _Keybinds = null;
	private Control _keybindsOptions = null;
	
//	An action in-game/associated button pressed dictionary
	private Godot.Collections.Dictionary _bindings = new Godot.Collections.Dictionary();
	
//	An action in-game/associated row in UI dictionary
	private Godot.Collections.Dictionary<string, Keybind> _keybinds = new Godot.Collections.Dictionary<string, Keybind>();
	
	public override void _Ready()
	{
		_Keybinds = GetNode("/root/Keybinds") as Keybinds;

		_bindings = _Keybinds.Bindings.Duplicate();
		
		_keybindsOptions = GetNode("Details") as Control;
		
		var keybindScene = GD.Load(KEYBIND_PATH) as PackedScene;
		var topFrameScene = GD.Load(TOP_FRAME_PATH) as PackedScene;
		var hSplitScene = GD.Load(H_SPLIT_PATH) as PackedScene;
		var downFrameScene = GD.Load(DOWN_FRAME_PATH) as PackedScene;
		
		var i = 0;
		
		foreach(String key in _bindings.Keys)
		{
			var keybind = keybindScene.Instance() as Keybind;
			Control frame;
			
			if (i==0)
			{
				keybind.MarginTop = 8;
				keybind.MarginBottom = -2;
				frame = topFrameScene.Instance() as Control;
			}
			else if (i==_bindings.Keys.Count-1)
			{
				keybind.MarginTop = 3;
				keybind.MarginBottom = -7;
				frame = downFrameScene.Instance() as Control;
			}
			else
			{
				keybind.MarginTop = 2;
				keybind.MarginBottom = -2;
				frame = hSplitScene.Instance() as Control;
			}
			
			_keybindsOptions.AddChild(frame);
			frame.AddChild(keybind);
			
			keybind.SetKeyValue(key, Convert.ToUInt32(_bindings[key]));
			_keybinds[key] = keybind;
			keybind.Connect("ChangeBind", this, "_changeBind");
			
			i+=1;
		}
	}

	public override void Save()
	{
		_Keybinds.Bindings = _bindings.Duplicate();
		_Keybinds.SetGameBinds();
		_Keybinds.WriteGameBinds();
	}
	
	private void _changeBind(String command, uint bind)
	{
		_bindings[command] = bind;
		foreach (String key in _bindings.Keys)
		{
			if (key != command && bind != (uint)0 && Convert.ToUInt32(_bindings[key]) == bind)
			{
				_bindings[key] = 0;
				_keybinds[key].SetKeyValue(key, (uint)0);
			}
		}
	}
}

using Godot;
using System;

public class OptionsMenu : Control
{	
	private LevelManager _LevelManager = null;
	private TabContainer _tabContainer = null;
	
	private Fade _fadeIn = null;
	
	public override void _Ready()
	{
		_LevelManager = GetNode("/root/LevelManager") as LevelManager;
		_tabContainer = GetNode("Rows/MiddleRow/TabContainer") as TabContainer;
		_fadeIn = GetNode("Fade") as Fade;
	}
	
	private void _on_SaveButton_pressed()
	{
		var tab = _tabContainer.GetChildren()[_tabContainer.CurrentTab] as Tab;
		tab.Save();
	}
	
	private async void _on_BackButton_pressed()
	{
		_fadeIn.Play();
		await ToSignal(_fadeIn, "FadeFinished");
		_LevelManager.GoToMainMenu();
	}
} 


using Godot;
using System;

public class PauseOverlay : Control
{
	[Signal]
	delegate void Resume();
	
	private LevelManager _LevelManager = null;
	
	public override void _Ready()
	{
		_LevelManager = GetNode("/root/LevelManager") as LevelManager;
	}
	
	public void Display()
	{
		Visible = true;
		GetTree().Paused = true;
	}
	
	public void Cancel()
	{
		Visible = false;
		GetTree().Paused = false;
	}
	
//	SIGNALS
	
	private void _on_ResumeButton_pressed()
	{
		EmitSignal("Resume");
	}
	
	private void _on_MainMenuButton_pressed()
	{
		GetTree().Paused = false;
		_LevelManager.GoToMainMenu();
	}
	
	private void _on_QuitButton_pressed()
	{
		GetTree().Quit();
	}
}


using Godot;
using System;

public class LevelClearedOverlay : Control
{
	private LevelManager _LevelManager = null;
	private Score _Score = null;
	private Stats _Stats = null;
	
	private Label _title = null;
	private Control _fullHeartContainer = null;
	private Control _halfHeartContainer = null;
	private Control _healthContainer = null;
	private Label _timeLabel = null;
	private Label _hitsTakenLabel = null;
	private Label _weaponsUsedLabel = null;
	
	private Fade _fade = null;
	private bool _inTransition = false;
	
	public override void _Ready()
	{
		_title = GetNode("MarginContainer/Rows/UpperRow/PaperBackground/Title") as Label;
		_fullHeartContainer = GetNode("MarginContainer/Rows/MiddleRow/Details/RemainingHealthContainer/HBoxContainer/Health/FullHeartContainer") as Control;
		_halfHeartContainer = GetNode("MarginContainer/Rows/MiddleRow/Details/RemainingHealthContainer/HBoxContainer/Health/HalfHeartContainer") as Control;
		_healthContainer = GetNode("MarginContainer/Rows/MiddleRow/Details/RemainingHealthContainer/HBoxContainer/Health") as Control;
		_timeLabel = GetNode("MarginContainer/Rows/MiddleRow/Details/TimeContainer/HBoxContainer/TimeValue") as Label;
		_hitsTakenLabel = GetNode("MarginContainer/Rows/MiddleRow/Details/HitsTakenContainer/HBoxContainer/HitsTakenValue") as Label;
		_weaponsUsedLabel = GetNode("MarginContainer/Rows/MiddleRow/Details/WeaponsUsedContainer/HBoxContainer/WeaponsUsedValue") as Label;
		_fade = GetNode("Fade") as Fade;
		
		_LevelManager = GetNode("/root/LevelManager") as LevelManager;
		_Score = GetNode("/root/Score") as Score;
		_Stats = GetNode("/root/PlayerStats") as Stats;
	}
	
	public void Display()
	{
		_title.Text = "Level " + _LevelManager.CurrentLevel + " cleared";
		
		_fullHeartContainer.RectMinSize = new Vector2(16*(_Stats.Health/2), 32);
		_halfHeartContainer.RectMinSize = new Vector2(16*(_Stats.Health/2 + _Stats.Health%2), 32);
		_healthContainer.RectMinSize = new Vector2(16*(_Stats.Health/2 + _Stats.Health%2), 32);
		_fullHeartContainer.Visible = _Stats.Health>1;
		
		var minutes = (int)_Score.Time / 60;
		var seconds = (int)_Score.Time % 60;
		_timeLabel.Text = String.Format("{0}:{1:D2} s", minutes, seconds);
		
		_hitsTakenLabel.Text = _Score.HitsTaken.ToString();
		
		_weaponsUsedLabel.Text = _Score.WeaponsUsed.ToString();
		
		Visible = true;
	}
	
	private async void _on_NextLevelButton_pressed()
	{
		if (!_inTransition)
		{
			_inTransition = true;
			_fade.Play();
			await ToSignal(_fade, "FadeFinished");
			_LevelManager.NextLevel();
		}
	}
	
	private async void _on_MenuButton_pressed()
	{
		if (!_inTransition)
		{
			_inTransition = true;
			_fade.Play();
			await ToSignal(_fade, "FadeFinished");
			_LevelManager.GoToMainMenu();
		}
	}
}

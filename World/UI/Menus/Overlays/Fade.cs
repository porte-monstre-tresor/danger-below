using Godot;
using System;

public class Fade : ColorRect
{
	[Signal]
	delegate void FadeFinished();
	
	[Export(PropertyHint.Enum, "In, Out")]
	private int _fadeType = 0;
	public int FadeType
	{
		get { return _fadeType; }
		set
		{
			Color = new Color((value == 0) ? "#00000000" : "#000000");
			_fadeType = value;
		}
	}
	
	private AnimationPlayer _animationPlayer = null;
	
	public override void _Ready()
	{
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
		Color = new Color((FadeType == 0) ? "#00000000" : "#000000");
	}
	
	public void Play()
	{
		_animationPlayer.Play((FadeType==0) ? "FadeIn" : "FadeOut");
	}
}

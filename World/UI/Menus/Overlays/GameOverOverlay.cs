using Godot;
using System;

public class GameOverOverlay : Control
{
	private LevelManager _LevelManager = null;
	private Stats _Stats = null;
	private Score _Score = null;
	
	private Label _killedByLabel = null;
	private Label _timeLabel = null;
	private Label _hitsTakenLabel = null;
	private Label _weaponsUsedLabel = null;
	
	private Fade _fade = null;
	
	public override void _Ready()
	{
		_killedByLabel = GetNode("MarginContainer/Rows/MiddleRow/Details/KilledByContainer/HBoxContainer/KilledByValue") as Label;
		_timeLabel = GetNode("MarginContainer/Rows/MiddleRow/Details/TimeContainer/HBoxContainer/TimeValue") as Label;
		_hitsTakenLabel = GetNode("MarginContainer/Rows/MiddleRow/Details/HitsTakenContainer/HBoxContainer/HitsTakenValue") as Label;
		_weaponsUsedLabel = GetNode("MarginContainer/Rows/MiddleRow/Details/WeaponsUsedContainer/HBoxContainer/WeaponsUsedValue") as Label;
		_fade = GetNode("Fade") as Fade;
		
		_Stats = GetNode("/root/PlayerStats") as Stats;
		_Score = GetNode("/root/Score") as Score;
		_LevelManager = GetNode("/root/LevelManager") as LevelManager;
	}
	
	public void Display()
	{
		_killedByLabel.Text = _Score.KilledBy;
		
		var minutes = (int)_Score.Time / 60;
		var seconds = (int)_Score.Time % 60;
		_timeLabel.Text = String.Format("{0}:{1:D2} s", minutes, seconds);
		
		_hitsTakenLabel.Text = _Score.HitsTaken.ToString();
		
		_weaponsUsedLabel.Text = _Score.WeaponsUsed.ToString();
		
		Visible = true;
	}

	private async void _on_BackButton_pressed()
	{
		_fade.Play();
		await ToSignal(_fade, "FadeFinished");
		_LevelManager.GoToMainMenu();
	}
	
	private async void _on_QuitButton_pressed()
	{
		_fade.Play();
		await ToSignal(_fade, "FadeFinished");
		GetTree().Quit();
		
	}

	private async void _on_LobbyButton_pressed()
	{
		_fade.Play();
		await ToSignal(_fade, "FadeFinished");
		_LevelManager.SpawnInLobby();
	}
}


using Godot;
using System;

public class MainMenu : Control
{
	private LevelManager _LevelManager = null;
	
	private Fade _fadeIn = null;
	
	public override void _Ready()
	{
		_LevelManager = GetNode("/root/LevelManager") as LevelManager;
		
		_fadeIn = GetNode("FadeIn") as Fade;
	}

	private async void _on_PlayMenuButton_pressed()
	{
		_fadeIn.Play();
		await ToSignal(_fadeIn, "FadeFinished");
		
		var saveGame = new File();
		if (!saveGame.FileExists("user://savegame.save"))
		{
			_LevelManager.StartTutorial();
		}
		else
		{
			_LevelManager.SpawnInLobby();
		}
	}

	private async void _on_OptionsMenuButton_pressed()
	{
		_fadeIn.Play();
		await ToSignal(_fadeIn, "FadeFinished");
		_LevelManager.GoToOptionsMenu();
	}

	private void _on_QuitMenuButton_pressed()
	{
		GetTree().Quit();
	}
}


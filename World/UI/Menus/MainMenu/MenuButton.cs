using Godot;
using System;

public class MenuButton : Button
{	
	private AnimationPlayer _animationPlayer = null;
	
	public override void _Ready()
	{
		_animationPlayer = GetNode("AnimationPlayer") as AnimationPlayer;
	}
	
	private void _on_MenuButton_button_down()
	{
		_animationPlayer.Play("Press");
	}

	private void _on_MenuButton_button_up()
	{
		_animationPlayer.Play("Release");
	}
}


